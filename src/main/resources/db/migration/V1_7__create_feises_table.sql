CREATE TABLE IF NOT EXISTS feises (
    id SERIAL PRIMARY KEY,
    feis_name VARCHAR NOT NULL UNIQUE,
    feis_type VARCHAR NOT NULL,
    feis_start_date VARCHAR NOT NULL,
    feis_end_date VARCHAR NOT NULL,
    school_id INT REFERENCES schools
);