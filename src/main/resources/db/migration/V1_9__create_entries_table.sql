CREATE TABLE IF NOT EXISTS entries (
    id SERIAL PRIMARY KEY,
    entry_feis_number INT,
    entry_date VARCHAR NOT NULL,
    student_id INT REFERENCES students,
    feis_id INT REFERENCES feises,
    UNIQUE (student_id, feis_id)
);

CREATE TABLE IF NOT EXISTS entry_items (
    id SERIAL PRIMARY KEY,
    entry_id INT REFERENCES entries,
    solo_dance_id INT REFERENCES solo_dances
);
