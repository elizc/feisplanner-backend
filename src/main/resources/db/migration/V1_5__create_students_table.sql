CREATE TABLE IF NOT EXISTS students (
    id SERIAL PRIMARY KEY,
    student_first_name VARCHAR NOT NULL,
    student_surname VARCHAR NOT NULL,
    student_birth_date VARCHAR NOT NULL,
    student_gender VARCHAR NOT NULL,
    student_reel_level VARCHAR NOT NULL,
    student_light_level VARCHAR NOT NULL,
    student_slip_level VARCHAR NOT NULL,
    student_single_level VARCHAR NOT NULL,
    student_treble_level VARCHAR NOT NULL,
    student_hornpipe_level VARCHAR NOT NULL,
    student_trad_set_level VARCHAR NOT NULL,
    student_won_prelims_count INT NOT NULL,
    user_id INT REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE
);
