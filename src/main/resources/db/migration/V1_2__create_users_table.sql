CREATE TABLE IF NOT EXISTS roles (
    id SERIAL PRIMARY KEY,
    role_name VARCHAR NOT NULL
);

INSERT INTO roles (id, role_name) VALUES
(0, 'user'),
(1, 'admin')
ON CONFLICT DO NOTHING;

CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    user_email VARCHAR NOT NULL UNIQUE,
    user_password VARCHAR NOT NULL,
    user_first_name VARCHAR NOT NULL,
    user_surname VARCHAR NOT NULL,
    role_id INT REFERENCES roles,
    school_id INT REFERENCES schools
);

/*admin's password: 9McK8FjF6BC4auJp*/
INSERT INTO users (id, user_email, user_password, user_first_name, user_surname, role_id, school_id) VALUES
(1, 'eliz.cazankova@gmail.com', '$2y$10$16ovn5P7szguYN/LNe8ifeZo2.2iS.z5DwfOX4c7i9wRnXJkTRp.O', 'Elizaveta', 'Kazankova', 1, 1)
ON CONFLICT DO NOTHING;