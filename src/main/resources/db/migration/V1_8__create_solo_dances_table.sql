CREATE TABLE IF NOT EXISTS solo_dances_types (
    id INT PRIMARY KEY,
    solo_dance_type_name VARCHAR NOT NULL
);

INSERT INTO solo_dances_types (id, solo_dance_type_name) VALUES
(1, 'Reel'),
(2, 'Light Jig'),
(3, 'Slip Jig'),
(4, 'Single Jig'),
(5, 'Treble Jig'),
(6, 'Hornpipe'),
(7, 'Trad. Set'),
(8, 'Premiership'),
(9, 'Preliminary'),
(10, 'Championship');

CREATE TABLE IF NOT EXISTS levels (
    id INT PRIMARY KEY,
    level_name VARCHAR NOT NULL
);

INSERT INTO levels (id, level_name) VALUES
(1, 'Beginner'),
(2, 'Primary'),
(3, 'Intermediate'),
(4, 'Open');                                          
                                           
CREATE TABLE IF NOT EXISTS solo_dances (
    id INT PRIMARY KEY,
    level_id INT REFERENCES levels,
    solo_dance_type_id INT REFERENCES solo_dances_types
);

INSERT INTO solo_dances (id, level_id, solo_dance_type_id) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 3, 1),
(5, 1, 2),
(6, 2, 2),
(7, 3, 2),
(8, 1, 3),
(9, 2, 3),
(10, 3, 3),
(11, 3, 3),
(12, 1, 4),
(13, 2, 4),
(14, 3, 4),
(15, 1, 5),
(16, 2, 5),
(17, 3, 5),
(18, 3, 5),
(19, 1, 6),
(20, 2, 6),
(21, 3, 6),
(22, 3, 6),
(23, 1, 7),
(24, 2, 7),
(25, 3, 7),
(26, 3, 7),
(27, 1, 8),
(28, 2, 8),
(29, 3, 8),
(30, 3, 9),
(31, 3, 10);
