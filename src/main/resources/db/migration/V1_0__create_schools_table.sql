CREATE TABLE IF NOT EXISTS schools (
    id SERIAL PRIMARY KEY,
    school_name VARCHAR NOT NULL,
    school_region VARCHAR NOT NULL,
    school_city VARCHAR NOT NULL
);