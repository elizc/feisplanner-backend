package it.elizc.feisplanner.core.exception;

public class UserExistsException extends Exception {
    private String email;

    public UserExistsException(String email) {
        this.email = email;
    }

    @Override
    public String getMessage() {
        return "The user with email " + email + " already exists";
    }
}
