package it.elizc.feisplanner.core.exception;

public class EntryExistsException extends Exception {
    public EntryExistsException() {

    }

    @Override
    public String getMessage() {
        return "The feis with these student and feis already exists";
    }
}
