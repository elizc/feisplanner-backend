package it.elizc.feisplanner.core.exception;

public class SchoolExistsException extends Exception {
    private String schoolName;

    public SchoolExistsException(String schoolName) {
        super();
        this.schoolName = schoolName;
    }

    @Override
    public String getMessage() {
        return "The school " + schoolName + " already exists";
    }
}
