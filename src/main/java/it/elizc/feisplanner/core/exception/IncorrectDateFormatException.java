package it.elizc.feisplanner.core.exception;

public class IncorrectDateFormatException extends Exception {
    private String date;

    public IncorrectDateFormatException(String date) {
        this.date = date;
    }

    @Override
    public String getMessage() {
        return "Incorrect format of date " + date;
    }
}
