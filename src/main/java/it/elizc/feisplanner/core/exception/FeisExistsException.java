package it.elizc.feisplanner.core.exception;

public class FeisExistsException extends Exception {
    private String name;

    public FeisExistsException(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        return "The feis with name " + name + " already exists";
    }
}
