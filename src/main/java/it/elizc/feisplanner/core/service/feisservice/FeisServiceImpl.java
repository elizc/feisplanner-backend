package it.elizc.feisplanner.core.service.feisservice;

import it.elizc.feisplanner.core.exception.FeisExistsException;
import it.elizc.feisplanner.core.exception.IncorrectDateFormatException;
import it.elizc.feisplanner.core.model.Feis;
import it.elizc.feisplanner.core.repository.feisrepository.FeisRepository;
import org.springframework.dao.DataAccessException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FeisServiceImpl implements FeisService {
    private final FeisRepository feisRepository;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
    private Comparator<Feis> compareByDate = (feis1, feis2) -> {
        Date startDate1 = null;
        Date startDate2 = null;
        try {
            startDate1 = dateFormat.parse(feis1.getStartDate());
            startDate2 = dateFormat.parse(feis2.getStartDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Objects.requireNonNull(startDate1).compareTo(Objects.requireNonNull(startDate2));
    };

    public FeisServiceImpl(final FeisRepository feisRepository) {
        this.feisRepository = feisRepository;
    }

    @Override
    public List<Feis> getUnfinishedFeises() {
        Date now = new Date();
        Predicate<Feis> isUnfinished = feis -> {
            Date startDate = null;
            try {
                startDate = dateFormat.parse(feis.getStartDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return Objects.requireNonNull(startDate).compareTo(now) > 0;
        };

        List<Feis> allFeises = feisRepository.getFeises();
        return allFeises
                .stream()
                .filter(isUnfinished)
                .sorted(compareByDate)
                .collect(Collectors.toList());
    }

    @Override
    public List<Feis> getOpenedFeises() {
        Date now = new Date();
        Predicate<Feis> isOpened = feis -> {
            Date startDate = null;
            try {
                startDate = dateFormat.parse(feis.getStartDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long millisDiff = Objects.requireNonNull(startDate).getTime() - now.getTime();
            int daysDiff = (int) (millisDiff / (24 * 60 * 60 * 1000));
            return daysDiff > 14;
        };

        List<Feis> allFeises = feisRepository.getFeises();
        return allFeises
                .stream()
                .filter(isOpened)
                .sorted(compareByDate)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getFeisNamesByUserId(String userId) {
        List<Feis> feises = feisRepository.getFeisesByUser(Integer.parseInt(userId));
        return feises
                .stream()
                .sorted(compareByDate)
                .map(Feis::getName)
                .collect(Collectors.toList());
    }

    @Override
    public Feis addFeis(String name, String type, String startDate, String endDate, String schoolName)
            throws FeisExistsException {
        try {
            return feisRepository.addFeis(name, type, startDate, endDate, schoolName);
        } catch (DataAccessException e) {
            throw new FeisExistsException(name);
        }
    }

    @Override
    public Feis editFeis(String id, String name, String type, String startDate, String endDate, String schoolName)
            throws FeisExistsException {
        try {
            return feisRepository.editFeis(id, name, type, startDate, endDate, schoolName);
        } catch (DataAccessException e) {
            throw new FeisExistsException(name);
        }
    }

    @Override
    public Feis deleteFeis(String id) {
        return feisRepository.deleteFeis(Integer.parseInt(id));
    }
}
