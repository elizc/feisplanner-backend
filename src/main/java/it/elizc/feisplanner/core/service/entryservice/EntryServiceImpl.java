package it.elizc.feisplanner.core.service.entryservice;

import it.elizc.feisplanner.core.exception.EntryExistsException;
import it.elizc.feisplanner.core.model.Entry;
import it.elizc.feisplanner.core.model.Student;
import it.elizc.feisplanner.core.repository.entryrepository.EntryRepository;
import it.elizc.feisplanner.web.request.AddSoloDanceRequest;
import org.springframework.dao.DataAccessException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class EntryServiceImpl implements EntryService {
    private final EntryRepository entryRepository;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
    private final Comparator<Entry> compareByFeisDate = (entry1, entry2) -> {
        Date startDate1 = null;
        Date startDate2 = null;
        try {
            startDate1 = dateFormat.parse(entry1.getFeis().getStartDate());
            startDate2 = dateFormat.parse(entry2.getFeis().getStartDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Objects.requireNonNull(startDate1).compareTo(Objects.requireNonNull(startDate2));
    };
    private final Comparator<Student> compareBySchoolName = (dancer1, dancer2) -> {
        String schoolName1 = dancer1.getTeacher().getSchool().getName();
        String schoolName2 = dancer2.getTeacher().getSchool().getName();
        return schoolName1.compareTo(schoolName2);
    };
    private final Comparator<Entry> compareByStudentSurname = (entry1, entry2) -> {
        String surname1 = entry1.getStudent().getSurname();
        String surname2 = entry2.getStudent().getSurname();
        return surname1.compareTo(surname2);
    };
    private final Comparator<Entry> compareByStudentBirthDate = (entry1, entry2) -> {
        Date birthDate1 = null;
        Date birthDate2 = null;
        try {
            birthDate1 = dateFormat.parse(entry1.getStudent().getBirthDate());
            birthDate2 = dateFormat.parse(entry2.getStudent().getBirthDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Objects.requireNonNull(birthDate2).compareTo(Objects.requireNonNull(birthDate1));
    };

    public EntryServiceImpl(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    @Override
    public List<Entry> getEntriesByUserId(String userId) {
        return entryRepository
                .getEntriesByUserId(Integer.parseInt(userId))
                .stream()
                .sorted(compareByFeisDate)
                .collect(Collectors.toList());
    }

    @Override
    public List<Entry> getEntriesByFeisName(String feisName) {
        return entryRepository
                .getEntriesByFeisName(feisName)
                .stream()
                .sorted(compareByStudentSurname)
                .collect(Collectors.toList());
    }

    @Override
    public Entry addEntry(String feisId, String studentId, List<AddSoloDanceRequest> soloDances)
            throws EntryExistsException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        Date now = new Date();
        String creationDate = dateFormat.format(now);

        try {
            return entryRepository.addEntry(feisId, studentId, creationDate, soloDances);
        } catch (DataAccessException e) {
            throw new EntryExistsException();
        }
    }

    @Override
    public Entry editEntry(String id, String feisId, String studentId, List<AddSoloDanceRequest> soloDances) {
            return entryRepository.editEntry(
                    Integer.parseInt(id),
                    Integer.parseInt(feisId),
                    Integer.parseInt(studentId),
                    soloDances
            );
    }

    @Override
    public void deleteEntry(String id) {
        entryRepository.deleteEntry(Integer.parseInt(id));
    }

    @Override
    public void setNumbers(String feisName) {
        List<Entry> entries = entryRepository
                .getEntriesByFeisName(feisName)
                .stream()
                .sorted(compareByStudentBirthDate)
                .collect(Collectors.toList());

        for (int number = 1; number <= entries.size(); number++) {
            entries.get(number - 1).setFeisNumber(number);
        }

        entryRepository.setNumbers(entries);
    }

    @Override
    public boolean numbersAreSet(String feisName) {
        return entryRepository.numbersAreSet(feisName);
    }
}
