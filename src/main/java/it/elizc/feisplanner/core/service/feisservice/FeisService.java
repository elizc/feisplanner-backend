package it.elizc.feisplanner.core.service.feisservice;

import it.elizc.feisplanner.core.exception.FeisExistsException;
import it.elizc.feisplanner.core.model.Feis;

import java.util.List;

public interface FeisService {
    List<Feis> getUnfinishedFeises();

    List<Feis> getOpenedFeises();

    List<String> getFeisNamesByUserId(String userId);

    Feis addFeis(String name, String type, String startDate, String endDate, String schoolName) throws FeisExistsException;

    Feis editFeis(String id, String name, String type, String startDate, String endDate, String schoolName)
            throws FeisExistsException;

    Feis deleteFeis(String id);
}
