package it.elizc.feisplanner.core.service.schoolservice;

import it.elizc.feisplanner.core.exception.SchoolExistsException;
import it.elizc.feisplanner.core.model.School;

import java.util.List;

public interface SchoolService {
    School addSchool(String name, String region, String city) throws SchoolExistsException;

    List<School> getSchools();

    List<String> getSchoolsNames();

    School editSchool(School editedSchool) throws SchoolExistsException;

    School deleteSchool(String id);
}
