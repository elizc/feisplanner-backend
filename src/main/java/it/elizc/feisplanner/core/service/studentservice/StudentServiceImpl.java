package it.elizc.feisplanner.core.service.studentservice;

import it.elizc.feisplanner.core.model.Student;
import it.elizc.feisplanner.core.repository.studentrepository.StudentRepository;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> getStudentsByTeacher(String userId) {
        return studentRepository.getStudentsByUser(Integer.parseInt(userId));
    }

    @Override
    public Student addStudent(String firstName, String surname, String gender, String birthDate,
                              String reelLevel, String lightLevel, String slipLevel, String singleLevel, String trebleLevel,
                              String hornpipeLevel, String tradSetLevel, int wonPrelimsCount, String userId) {
        return studentRepository.addStudent(firstName, surname, gender, birthDate,
                reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                tradSetLevel, wonPrelimsCount, userId);
    }

    @Override
    public Student editStudent(Student editedStudent) {
        return studentRepository.editStudent(editedStudent);
    }

    @Override
    public Student deleteStudent(String id) {
        return studentRepository.deleteStudent(Integer.parseInt(id));
    }
}
