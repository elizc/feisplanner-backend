package it.elizc.feisplanner.core.service.entryservice;

import it.elizc.feisplanner.core.exception.EntryExistsException;
import it.elizc.feisplanner.core.model.Entry;
import it.elizc.feisplanner.web.request.AddSoloDanceRequest;

import java.util.List;

public interface EntryService {
    List<Entry> getEntriesByUserId(String userId);

    List<Entry> getEntriesByFeisName(String feisName);

    Entry addEntry(String feisId, String studentId, List<AddSoloDanceRequest> soloDances) throws EntryExistsException;

    Entry editEntry(String id, String feisId, String studentId, List<AddSoloDanceRequest> soloDances);

    void deleteEntry(String id);

    void setNumbers(String feisName);

    boolean numbersAreSet(String feisName);
}
