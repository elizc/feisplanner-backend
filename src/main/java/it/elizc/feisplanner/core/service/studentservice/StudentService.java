package it.elizc.feisplanner.core.service.studentservice;

import it.elizc.feisplanner.core.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getStudentsByTeacher(String userId);

    Student addStudent(String firstName, String surname, String gender, String birthDate,
                       String reelLevel, String lightLevel, String slipLevel, String singleLevel, String trebleLevel,
                       String hornpipeLevel, String tradSetLevel, int wonPrelimsCount, String userId);

    Student editStudent(Student editedStudent);

    Student deleteStudent(String id);
}
