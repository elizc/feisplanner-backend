package it.elizc.feisplanner.core.service.userservice;

import it.elizc.feisplanner.core.exception.UserExistsException;
import it.elizc.feisplanner.core.model.User;

public interface UserDataService {
    User addUser(String email, String password, String firstName, String surname, String schoolName) throws UserExistsException;

    User getUserByEmail(String email);
}
