package it.elizc.feisplanner.core.service.wordservice;

import it.elizc.feisplanner.core.model.Category;
import it.elizc.feisplanner.core.model.Entry;

import java.util.List;

public interface WordService {
    String getStageLists(List<Category> categories);

    String getNumbers(List<Entry> entries);

    String getStickers(List<Entry> entries);
}
