package it.elizc.feisplanner.core.service.userservice;

import it.elizc.feisplanner.core.exception.UserExistsException;
import it.elizc.feisplanner.core.model.User;
import it.elizc.feisplanner.core.repository.userrepository.UserRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserServiceImpl(
            final UserRepository userRepository,
            final PasswordEncoder encoder
    ) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Override
    public User addUser(String email, String password, String firstName, String surname, String schoolName)
            throws UserExistsException {
        try {
            return userRepository.addUser(email, encoder.encode(password), firstName, surname, schoolName);
        } catch (DataAccessException e) {
            throw new UserExistsException(email);
        }
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.getUserByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid email or password");
        }
        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                getAuthority(user)
        );
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(String.format("ROLE_%s", user.getRole().toUpperCase())));
        return authorities;
    }
}
