package it.elizc.feisplanner.core.service.excelservice;

import it.elizc.feisplanner.core.model.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ExcelServiceImpl implements ExcelService {
    private Workbook book;

    @Override
    public String generateDancersReport(List<Entry> entries) {
        Predicate<EntryItem> hasSoloDance = entryItem -> {
            Dance dance = entryItem.getDance();
            return dance instanceof SoloDance && !dance.isCupDance();
        };
        Predicate<EntryItem> hasCupDance = entryItem -> {
            Dance dance = entryItem.getDance();
            return dance.isCupDance();
        };

        book = new XSSFWorkbook();
        Sheet sheet = book.createSheet("All dancers");

        // Title
        Row titleRow = sheet.createRow(0);
        Cell reportNameCell = titleRow.createCell(0);
        Feis feis = entries.get(0).getFeis();
        reportNameCell.setCellValue(feis.getName() + ", " + feis.getStartDate() + " - ALL DANCERS REPORT");

        Font reportNameFont = book.createFont();
        reportNameFont.setBold(true);
        reportNameFont.setFontHeightInPoints((short) 13);

        CellStyle reportNameStyle = book.createCellStyle();
        reportNameStyle.setFont(reportNameFont);
        reportNameCell.setCellStyle(reportNameStyle);

        // Header
        Row headerRow = sheet.createRow(3);
        CellStyle standardCellStyle = getStandardCellStyle();
        CellStyle headerStyle = makeHeaderStyle(standardCellStyle);
        final String[] COLUMN_TITLES = {"Comp #", "Dancer", "DOB", "Age", "Gender", "School", "Solo Dances", "Cup Dances"};
        for (int i = 0; i < COLUMN_TITLES.length; i++) {
            Cell headerCell = headerRow.createCell(i + 1);
            headerCell.setCellValue(COLUMN_TITLES[i]);
            headerCell.setCellStyle(headerStyle);
            sheet.autoSizeColumn(i + 1);
        }

        // Body
        int entryRowIndex = 4;
        CellStyle centerAlignment = makeCenterAlignment(standardCellStyle);

        for (Entry entry : entries) {
            Row entryRow = sheet.createRow(entryRowIndex++);

            Cell numberCell = entryRow.createCell(1);
            numberCell.setCellValue(entry.getFeisNumber());
            numberCell.setCellStyle(centerAlignment);

            Student dancer = entry.getStudent();
            Cell dancerCell = entryRow.createCell(2);
            dancerCell.setCellValue(dancer.getSurname() + " " + dancer.getFirstName());
            dancerCell.setCellStyle(standardCellStyle);
            sheet.autoSizeColumn(2);

            Cell birthDateCell = entryRow.createCell(3);
            birthDateCell.setCellValue(dancer.getBirthDate());
            birthDateCell.setCellStyle(standardCellStyle);
            sheet.autoSizeColumn(3);

            Cell ageCell = entryRow.createCell(4);
            ageCell.setCellValue(dancer.getAge());
            ageCell.setCellStyle(centerAlignment);

            Cell genderCell = entryRow.createCell(5);
            genderCell.setCellValue(dancer.getGender().substring(0, 1).toUpperCase());
            genderCell.setCellStyle(centerAlignment);

            Cell schoolCell = entryRow.createCell(6);
            schoolCell.setCellValue(dancer.getTeacher().getSchool().getName());
            schoolCell.setCellStyle(standardCellStyle);
            sheet.autoSizeColumn(6);

            Cell soloDancesCell = entryRow.createCell(7);
            soloDancesCell.setCellStyle(centerAlignment);
            List<EntryItem> entryItems = entry.getEntryItems();
            if (entryItems.stream().anyMatch(hasSoloDance)) {
                soloDancesCell.setCellValue("Y");
            }

            Cell cupDancesCell = entryRow.createCell(8);
            cupDancesCell.setCellStyle(centerAlignment);
            if (entryItems.stream().anyMatch(hasCupDance)) {
                cupDancesCell.setCellValue("Y");
            }
        }

        // File
        String reportName = "dancers_report_" + entries.get(0).getFeis().getName() +
                ".xlsx";
        File reportFile = null;
        try {
            reportFile = createFile(reportName);
            FileOutputStream outputStream = new FileOutputStream(reportFile);
            book.write(outputStream);

            outputStream.close();
            book.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download/" + Objects.requireNonNull(reportFile).getName();
    }

    @Override
    public String generateScoringTable(List<Category> categories) {
        Predicate<Category> hasSoloDance = entryItem -> {
            Dance dance = entryItem.getDance();
            return dance instanceof SoloDance && !dance.isCupDance();
        };
        Predicate<Category> hasCupDance = entryItem -> {
            Dance dance = entryItem.getDance();
            return dance.isCupDance();
        };

        List<? extends Category> soloCategories = categories.stream().filter(hasSoloDance).collect(Collectors.toList());
        List<? extends Category> cupCategories = categories.stream().filter(hasCupDance).collect(Collectors.toList());


        book = new XSSFWorkbook();
        Sheet[] sheets = {
                book.createSheet(Level.BEGINNER),
                book.createSheet(Level.PRIMARY),
                book.createSheet(Level.INTERMEDIATE),
                book.createSheet(Level.OPEN),
                book.createSheet(DanceName.PREMIERSHIP),
                book.createSheet(DanceName.CHAMPIONSHIP),
                book.createSheet(DanceName.PRELIMINARY)
        };

        CellStyle standardCellStyle = getStandardCellStyle();
        CellStyle headerStyle = makeHeaderStyle(standardCellStyle);
        CellStyle columnNameStyle = makeColumnNameStyle(standardCellStyle);

        int sheetsIndex = 0;
        int rowIndex = 0;
        String categoryType = Level.BEGINNER;
        Sheet sheet = sheets[sheetsIndex];
        sheet.setColumnWidth(0, 255 * 10);
        sheet.setColumnWidth(1, 255 * 10);
        sheet.setColumnWidth(3, 255 * 35);
        sheet.setColumnWidth(4, 255 * 40);
        sheet.setColumnWidth(5, 255 * 50);

        for (Category category : soloCategories) {
            SoloCategory soloCategory = (SoloCategory) category;
            SoloDance dance = (SoloDance) soloCategory.getDance();
            if (!dance.getLevel().equals(categoryType)) {
                sheet = sheets[++sheetsIndex];
                sheet.setColumnWidth(0, 255 * 10);
                sheet.setColumnWidth(1, 255 * 10);
                sheet.setColumnWidth(3, 255 * 35);
                sheet.setColumnWidth(4, 255 * 40);
                sheet.setColumnWidth(5, 255 * 50);
                categoryType = dance.getLevel();
                rowIndex = 0;
            }

            // Header
            Row headerRow = sheet.createRow(rowIndex);
            String[] columnNames = {"Place", "Score", "Number", "Name", "School", "Comments"};
            for (int i = 0; i < columnNames.length; i++) {
                Cell headerCell = headerRow.createCell(i);

                if (i == 0) {
                    String shortenedCategoryName = getShortenedCategoryName(dance, category.getAgeDesignation());
                    headerCell.setCellValue(shortenedCategoryName);
                    sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex++, 0, 1));
                }

                if (i == 3) {
                    headerCell.setCellValue(dance.getName());
                }

                headerCell.setCellStyle(headerStyle);
            }

            Row columnNamesRow = sheet.createRow(rowIndex++);
            for (int i = 0; i < columnNames.length; i++) {
                Cell columnNameCell = columnNamesRow.createCell(i);
                columnNameCell.setCellValue(columnNames[i]);
                columnNameCell.setCellStyle(columnNameStyle);
            }

            // Body
            for (Entry entry : soloCategory.getEntries()) {
                Row entryRow = sheet.createRow(rowIndex++);
                for (int i = 0; i < columnNames.length; i++) {
                    Cell entryCell = entryRow.createCell(i);

                    if (i == 2) {
                        entryCell.setCellValue(entry.getFeisNumber());
                    }

                    if (i == 3) {
                        Student dancer = entry.getStudent();
                        entryCell.setCellValue(dancer.getSurname() + ", " + dancer.getFirstName());
                    }

                    if (i == 4) {
                        School school = entry.getStudent().getTeacher().getSchool();
                        entryCell.setCellValue(school.getName());
                    }

                    entryCell.setCellStyle(standardCellStyle);
                }
            }

            rowIndex += 3;
        }

        for (Category category : cupCategories) {
            SoloCategory soloCategory = (SoloCategory) category;
            SoloDance dance = (SoloDance) soloCategory.getDance();
            if (!dance.getName().equals(categoryType)) {
                sheet = sheets[++sheetsIndex];
                sheet.setColumnWidth(0, 255 * 10);
                sheet.setColumnWidth(1, 255 * 10);
                sheet.setColumnWidth(2, 255 * 35);
                sheet.setColumnWidth(3, 255 * 40);
                sheet.setColumnWidth(7, 255 * 50);
                categoryType = dance.getName();
                rowIndex = 0;
            }

            // Header
            Row headerRow = sheet.createRow(rowIndex);
            String[] columnNames = {"Place", "Number", "Name", "School",
                    "Judge A", "Judge B", "Judge C", "Comments"};
            for (int i = 0; i < columnNames.length; i++) {
                Cell headerCell = headerRow.createCell(i);

                if (i == 0) {
                    String shortenedCategoryName = getShortenedCategoryName(dance, category.getAgeDesignation());
                    headerCell.setCellValue(shortenedCategoryName);
                    sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex++, 0, 1));
                }

                if (i == 2) {
                    headerCell.setCellValue(dance.getName());
                }

                if (i == 3) {
                    headerCell.setCellValue(dance.getLevel());
                }

                headerCell.setCellStyle(headerStyle);
            }

            Row columnNamesRow = sheet.createRow(rowIndex++);
            for (int i = 0; i < columnNames.length; i++) {
                Cell columnNameCell = columnNamesRow.createCell(i);
                columnNameCell.setCellValue(columnNames[i]);
                columnNameCell.setCellStyle(columnNameStyle);
            }

            // Body
            for (Entry entry : soloCategory.getEntries()) {
                Row entryRow = sheet.createRow(rowIndex++);
                for (int i = 0; i < columnNames.length; i++) {
                    Cell entryCell = entryRow.createCell(i);

                    if (i == 1) {
                        entryCell.setCellValue(entry.getFeisNumber());
                    }

                    if (i == 2) {
                        Student dancer = entry.getStudent();
                        entryCell.setCellValue(dancer.getSurname() + ", " + dancer.getFirstName());
                    }

                    if (i == 3) {
                        School school = entry.getStudent().getTeacher().getSchool();
                        entryCell.setCellValue(school.getName());
                    }

                    entryCell.setCellStyle(standardCellStyle);
                }
            }

            rowIndex += 3;
        }


        // File
        String fileName = "scoring_table_" + categories.get(0).getFeis().getName() +
                ".xlsx";
        File reportFile = null;
        try {
            reportFile = createFile(fileName);
            FileOutputStream outputStream = new FileOutputStream(reportFile);
            book.write(outputStream);

            outputStream.close();
            book.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download/" + Objects.requireNonNull(reportFile).getName();
    }

    private String getShortenedCategoryName(SoloDance dance, String ageDesignation) {
        String shortenedName;
        String name = dance.getName();
        if (name.contains(" ")) {
            shortenedName = name.split(" ")[0];
        } else {
            switch (name) {
                case DanceName.HORNPIPE: {
                    shortenedName = "Horn";
                    break;
                }
                case DanceName.PREMIERSHIP: {
                    shortenedName = name.substring(0, 4);
                    break;
                }
                case DanceName.CHAMPIONSHIP: {
                    shortenedName = name.substring(0, 5);
                    break;
                }
                case DanceName.PRELIMINARY: {
                    shortenedName = name.substring(0, 6);
                    break;
                }
                default: {
                    shortenedName = name;
                    break;
                }
            }
        }

        String shortenedLevel;
        String level = dance.getLevel();
        if (level.equals(Level.OPEN)) {
            shortenedLevel = level;
        } else {
            shortenedLevel = level.substring(0, 3);
        }

        return shortenedLevel + ". " + shortenedName + " " + ageDesignation;
    }

    private File createFile(String fileName) throws IOException {
        final String DIRECTORY_NAME = "D:/files";
        File directory = new File(DIRECTORY_NAME);
        directory.mkdir();

        File reportFile = new File(directory, fileName);
        reportFile.createNewFile();
        return reportFile;
    }

    private CellStyle makeColumnNameStyle(CellStyle style) {
        Font font = book.createFont();
        font.setBold(true);
        font.setItalic(true);
        font.setFontHeightInPoints((short) 12);

        CellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(style);
        newStyle.setFont(font);

        return newStyle;
    }

    private CellStyle makeHeaderStyle(CellStyle style) {
        Font font = book.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 16);

        CellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(style);
        newStyle.setFont(font);

        return newStyle;
    }

    private CellStyle makeCenterAlignment(CellStyle style) {
        CellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(style);
        newStyle.setAlignment(HorizontalAlignment.CENTER);
        return newStyle;
    }

    private CellStyle getStandardCellStyle() {
        CellStyle style = book.createCellStyle();

        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);

        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());

        return style;
    }
}
