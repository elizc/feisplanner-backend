package it.elizc.feisplanner.core.service.categoryservice;

import it.elizc.feisplanner.core.model.*;
import it.elizc.feisplanner.core.repository.entryrepository.EntryRepository;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CategoryServiceImpl implements CategoryService {
    private EntryRepository entryRepository;
    private final Comparator<Category> compareByDanceName = (category1, category2) -> {
        Map<String, Integer> namesOrder = new HashMap<>();
        namesOrder.put(DanceName.CHAMPIONSHIP, 1);
        namesOrder.put(DanceName.PRELIMINARY, 2);
        namesOrder.put(DanceName.REEL, 3);
        namesOrder.put(DanceName.LIGHT, 4);
        namesOrder.put(DanceName.SLIP, 5);
        namesOrder.put(DanceName.SINGLE, 6);
        namesOrder.put(DanceName.PREMIERSHIP, 7);
        namesOrder.put(DanceName.TREBLE, 8);
        namesOrder.put(DanceName.HORNPIPE, 9);
        namesOrder.put(DanceName.TRAD_SET, 10);

        String danceName1 = category1.getDance().getName();
        Integer orderNumber1 = namesOrder.get(danceName1);

        String danceName2 = category2.getDance().getName();
        Integer orderNumber2 = namesOrder.get(danceName2);

        return orderNumber1.compareTo(orderNumber2);
    };

    private final Comparator<Dance> compareByLevel = (dance1, dance2) -> {
        Map<String, Integer> levelsOrder = new HashMap<>();
        levelsOrder.put(Level.BEGINNER, 1);
        levelsOrder.put(Level.PRIMARY, 2);
        levelsOrder.put(Level.INTERMEDIATE, 3);
        levelsOrder.put(Level.OPEN, 4);

        SoloDance soloDance1 = (SoloDance) dance1;
        String level1 = soloDance1.getLevel();
        Integer orderNumber1 = levelsOrder.get(level1);

        SoloDance soloDance2 = (SoloDance) dance2;
        String level2 = soloDance2.getLevel();
        Integer orderNumber2 = levelsOrder.get(level2);

        return orderNumber1.compareTo(orderNumber2);
    };

    private final Comparator<Entry> compareByFeisNumber = (entry1, entry2) -> {
        Integer feisNumber1 = entry1.getFeisNumber();
        Integer feisNumber2 = entry2.getFeisNumber();
        return feisNumber1.compareTo(feisNumber2);
    };

    public CategoryServiceImpl(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    @Override
    public List<Category> categorize(String feisName) {
        Map<Dance, List<Entry>> entriesGroupedByDance = new HashMap<>();
        List<Entry> entries = entryRepository
                .getEntriesByFeisName(feisName)
                .stream()
                .sorted(compareByFeisNumber)
                .collect(Collectors.toList());

        Consumer<Entry> distributeEntriesToDances = entry -> {
            for (EntryItem entryItem : entry.getEntryItems()) {
                Dance dance = entryItem.getDance();
                if (!entriesGroupedByDance.containsKey(dance)) {
                    entriesGroupedByDance.put(dance, new ArrayList<>());
                }

                List<Entry> dancersList = entriesGroupedByDance.get(dance);
                dancersList.add(entry);
                entriesGroupedByDance.replace(dance, dancersList);
            }
        };

        entries.forEach(distributeEntriesToDances);
        List<Category> categories = new ArrayList<>();

        Feis feis = entries.get(0).getFeis();
        GregorianCalendar now = new GregorianCalendar();
        int currentYear = now.get(Calendar.YEAR);
        for (Map.Entry<Dance, List<Entry>> group : entriesGroupedByDance.entrySet()) {
            List<Entry> entriesInDance = group.getValue();
            SoloDance dance = (SoloDance) group.getKey();

            if (entriesInDance.size() < 10) {
                categories.add(new SoloCategory(feis, 0, Integer.MAX_VALUE, dance, entriesInDance));
                continue;
            }

            List<Entry> entriesInCategory = new ArrayList<>();
            SoloCategory previousCategory = null;
            for (int year = currentYear - 1; year >= 1960; year--) {
                Iterator<Entry> iterator = entriesInDance.iterator();
                while (iterator.hasNext()) {
                    Entry entry = iterator.next();
                    Student dancer = entry.getStudent();
                    int birthYear = Integer.parseInt(dancer.getBirthDate().substring(6));
                    if (birthYear == year) {
                        entriesInCategory.add(entry);
                        iterator.remove();
                    }
                }

                if (entriesInDance.size() < 5) {
                    entriesInCategory.addAll(new ArrayList<>(entriesInDance));
                    entriesInDance.clear();
                }

                if (entriesInCategory.size() >= 5) {
                    int minAge;
                    int maxAge;
                    SoloCategory category;
                    if (entriesInDance.isEmpty()) {
                        if (previousCategory == null) {
                            minAge = 0;
                        } else {
                            List<Entry> previousEntries = Objects.requireNonNull(previousCategory).getEntries();
                            minAge = previousEntries.get(previousEntries.size() - 1).getStudent().getAge() + 1;
                        }
                        maxAge = Integer.MAX_VALUE;
                        category = new SoloCategory(feis, minAge, maxAge, dance, new ArrayList<>(entriesInCategory));
                        previousCategory = null;
                    } else {
                        minAge = entriesInCategory.get(0).getStudent().getAge();
                        maxAge = entriesInCategory.get(entriesInCategory.size() - 1).getStudent().getAge();
                        category = new SoloCategory(feis, minAge, maxAge, dance, new ArrayList<>(entriesInCategory));
                        previousCategory = category;
                    }
                    categories.add(category);
                    entriesInCategory.clear();
                    if (entriesInDance.isEmpty()) break;
                }
            }
        }


        return categories
                .stream()
                .sorted(Comparator
                        .comparing(Category::getDance, compareByLevel)
                        .thenComparing(compareByDanceName))
                .collect(Collectors.toList());
    }
}
