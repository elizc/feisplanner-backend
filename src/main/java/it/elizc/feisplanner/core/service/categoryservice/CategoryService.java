package it.elizc.feisplanner.core.service.categoryservice;

import it.elizc.feisplanner.core.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> categorize(String feisName);
}
