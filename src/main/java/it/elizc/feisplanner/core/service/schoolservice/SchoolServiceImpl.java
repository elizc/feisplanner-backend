package it.elizc.feisplanner.core.service.schoolservice;

import it.elizc.feisplanner.core.exception.SchoolExistsException;
import it.elizc.feisplanner.core.model.School;
import it.elizc.feisplanner.core.repository.schoolrepository.SchoolRepository;
import org.springframework.dao.DataAccessException;

import java.util.List;

public class SchoolServiceImpl implements SchoolService {
    private SchoolRepository schoolRepository;

    public SchoolServiceImpl(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    @Override
    public School addSchool(String name, String region, String city) throws SchoolExistsException {
        try {
            return schoolRepository.addSchool(name, region, city);
        } catch (DataAccessException e) {
            throw new SchoolExistsException(name);
        }
    }

    @Override
    public List<School> getSchools() {
        return schoolRepository.getSchools();
    }

    @Override
    public List<String> getSchoolsNames() {
        return schoolRepository.getSchoolsNames();
    }

    @Override
    public School editSchool(School editedSchool) throws SchoolExistsException {
        try {
            return schoolRepository.editSchool(editedSchool);
        } catch (DataAccessException e) {
            throw new SchoolExistsException(editedSchool.getName());
        }
    }

    @Override
    public School deleteSchool(String id) {
        return schoolRepository.deleteSchool(Integer.parseInt(id));
    }
}
