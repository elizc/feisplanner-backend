package it.elizc.feisplanner.core.service.timatableservice;

import it.elizc.feisplanner.core.model.Category;
import it.elizc.feisplanner.core.model.Dance;
import it.elizc.feisplanner.core.model.Hall;

import java.util.List;
import java.util.Map;

public interface TimetableService {
    String generateTimetable(Map<Dance, List<Category>> categories, int startHours, int startMinutes, List<Hall> halls);
}
