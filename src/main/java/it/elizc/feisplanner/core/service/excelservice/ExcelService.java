package it.elizc.feisplanner.core.service.excelservice;

import it.elizc.feisplanner.core.model.Category;
import it.elizc.feisplanner.core.model.Entry;

import java.io.IOException;
import java.util.List;

public interface ExcelService {
    String generateDancersReport(List<Entry> entries) throws IOException;

    String generateScoringTable(List<Category> categories);
}
