package it.elizc.feisplanner.core.service.wordservice;

import it.elizc.feisplanner.core.model.*;
import org.apache.poi.xwpf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class WordServiceImpl implements WordService {
    private XWPFDocument doc;
    private final Comparator<Entry> compareByFeisNumber = (entry1, entry2) -> {
        Integer feisNumber1 = entry1.getFeisNumber();
        Integer feisNumber2 = entry2.getFeisNumber();
        return feisNumber1.compareTo(feisNumber2);
    };

    @Override
    public String getStageLists(List<Category> categories) {
        doc = new XWPFDocument();
        for (Category category : categories) {
            XWPFParagraph title = doc.createParagraph();
            XWPFRun titleConfig = title.createRun();
            titleConfig.setText(getCategoryName(category));
            titleConfig.setBold(true);
            titleConfig.setFontSize(20);

            for (Entry entry : ((SoloCategory) category).getEntries()) {
                XWPFParagraph dancerParagraph = doc.createParagraph();
                XWPFRun dancerConfig = dancerParagraph.createRun();
                dancerConfig.setText(getEntryInfo(entry));
                dancerConfig.setFontSize(16);
            }

            XWPFParagraph space = doc.createParagraph();
            XWPFRun spaceConfig = space.createRun();
            spaceConfig.setText("");
        }

        // File
        String feisName = categories.get(0).getFeis().getName();
        String docName = "stage lists " + feisName +
                ".docx";
        File file = null;
        try {
            file = createFile(docName);
            FileOutputStream outputStream = new FileOutputStream(file);
            doc.write(outputStream);

            outputStream.close();
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download/" + Objects.requireNonNull(file).getName();
    }

    @Override
    public String getNumbers(List<Entry> entries) {
        entries = entries
                .stream()
                .sorted(compareByFeisNumber)
                .collect(Collectors.toList());

        doc = new XWPFDocument();
        for (int i = 0; i < entries.size(); i++) {
            int spacesCount;
            if (i == 0) {
                spacesCount = 3;
            } else {
                spacesCount = 4;
            }

            for (int j = 1; j <= spacesCount; j++) {
                XWPFParagraph space = doc.createParagraph();
                XWPFRun spaceConfig = space.createRun();
                spaceConfig.setFontSize(10);
                spaceConfig.setFontFamily("Times New Roman");
            }

            XWPFParagraph number = doc.createParagraph();
            number.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun numberConfig = number.createRun();
            numberConfig.setText(Integer.toString(entries.get(i).getFeisNumber()));
            numberConfig.setBold(true);
            numberConfig.setFontSize(216);
            numberConfig.setFontFamily("Arial");
        }

        // File
        String feisName = entries.get(0).getFeis().getName();
        String docName = "numbers " + feisName +
                ".docx";
        File file = null;
        try {
            file = createFile(docName);
            FileOutputStream outputStream = new FileOutputStream(file);
            doc.write(outputStream);

            outputStream.close();
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download/" + Objects.requireNonNull(file).getName();
    }

    @Override
    public String getStickers(List<Entry> entries) {
        entries = entries
                .stream()
                .sorted(compareByFeisNumber)
                .collect(Collectors.toList());

        doc = new XWPFDocument();
        for (Entry entry : entries) {
            Student dancer = entry.getStudent();

            XWPFParagraph dancerParagraph = doc.createParagraph();
            XWPFRun dancerConfig = dancerParagraph.createRun();
            String dancerText = dancer.getFirstName() + " " + dancer.getSurname() + " - " + entry.getFeisNumber();
            dancerConfig.setText(dancerText);
            dancerConfig.setBold(true);
            dancerConfig.setFontSize(10);
            dancerConfig.setFontFamily("Arial");

            School school = dancer.getTeacher().getSchool();
            XWPFParagraph schoolParagraph = doc.createParagraph();
            XWPFRun schoolConfig = schoolParagraph.createRun();
            schoolConfig.setText(school.getName());
            schoolConfig.setFontSize(8);
            schoolConfig.setFontFamily("Arial");

            XWPFParagraph ageParagraph = doc.createParagraph();
            XWPFRun ageConfig = ageParagraph.createRun();
            ageConfig.setText(dancer.getBirthDate() + " / Age by rule: " + dancer.getAge());
            ageConfig.setFontSize(8);
            ageConfig.setFontFamily("Arial");

            for (EntryItem entryItem : entry.getEntryItems()) {
                SoloDance dance = (SoloDance) entryItem.getDance();
                XWPFParagraph danceParagraph = doc.createParagraph();
                XWPFRun danceConfig = danceParagraph.createRun();
                danceConfig.setText("  " + dance.getName() + " / " + dance.getLevel());
                danceConfig.setFontSize(8);
                danceConfig.setFontFamily("Arial");
            }

            XWPFParagraph spaceParagraph = doc.createParagraph();
            XWPFRun spaceConfig = spaceParagraph.createRun();
            spaceConfig.setFontSize(10);
            spaceConfig.setFontFamily("Arial");
        }

        // File
        String feisName = entries.get(0).getFeis().getName();
        String docName = "stickers " + feisName +
                ".docx";
        File file = null;
        try {
            file = createFile(docName);
            FileOutputStream outputStream = new FileOutputStream(file);
            doc.write(outputStream);

            outputStream.close();
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download/" + Objects.requireNonNull(file).getName();
    }

    private String getCategoryName(Category category) {
        String ageDesignation = category.getAgeDesignation();
        Dance dance = category.getDance();
        String danceName = dance.getName();
        String level = ((SoloDance) dance).getLevel();
        return level + " " + danceName + " " + ageDesignation;
    }

    private String getEntryInfo(Entry entry) {
        Student dancer = entry.getStudent();
        String firstName = dancer.getFirstName();
        String surname = dancer.getSurname();

        String schoolName = dancer.getTeacher().getSchool().getName();
        int number = entry.getFeisNumber();
        return number + " " + firstName + " " + surname + " - " + schoolName;
    }

    private File createFile(String fileName) throws IOException {
        final String DIRECTORY_NAME = "D:/files";
        File directory = new File(DIRECTORY_NAME);
        directory.mkdir();

        File reportFile = new File(directory, fileName);
        reportFile.createNewFile();
        return reportFile;
    }

}
