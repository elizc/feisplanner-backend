package it.elizc.feisplanner.core.service.timatableservice;

import it.elizc.feisplanner.core.model.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TimetableServiceImpl implements TimetableService {
    private Workbook book;

    @Override
    public String generateTimetable(
            Map<Dance, List<Category>> categories,
            int startHours,
            int startMinutes,
            List<Hall> halls
    ) {
        book = new XSSFWorkbook();
        Sheet sheet = book.createSheet("Day 1");
        sheet.setColumnWidth(1, 30 * 255);

        CellStyle standardCellStyle = getStandardCellStyle();
        CellStyle importantCellStyle = makeImportantCellStyle(standardCellStyle);
        int columnsCount = getColumnsCount(halls);
        int rowIndex = 0;

        //Header
        Row hallsRow = sheet.createRow(rowIndex++);
        final int MAIN_COLUMNS_COUNT = 2;
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, MAIN_COLUMNS_COUNT - 1));
        int startMergeIndex = MAIN_COLUMNS_COUNT;
        int hallIndex = 0;
        for (int i = 0; i < columnsCount; i++) {
            Cell hallCell = hallsRow.createCell(i);
            if (i == startMergeIndex) {
                Hall hall = halls.get(hallIndex);
                hallCell.setCellValue(hall.getName());
                int stagesCount = hall.getStagesCount();
                if (stagesCount > 1) {
                    sheet.addMergedRegion(new CellRangeAddress(0, 0, startMergeIndex, startMergeIndex + stagesCount - 1));
                }
                startMergeIndex = startMergeIndex + stagesCount;
                hallIndex++;
            }
            hallCell.setCellStyle(importantCellStyle);
        }

        Row stagesRow = sheet.createRow(rowIndex++);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, MAIN_COLUMNS_COUNT - 1));
        for (int i = 0; i < columnsCount; i++) {
            Cell stageCell = stagesRow.createCell(i);
            if (i >= MAIN_COLUMNS_COUNT) {
                stageCell.setCellValue("Stage " + (char) ('A' + i - MAIN_COLUMNS_COUNT));
            }
            stageCell.setCellStyle(importantCellStyle);
        }

        // Body
        LocalTime time = LocalTime.of(startHours - 1, startMinutes);
        Row registrationRow = sheet.createRow(rowIndex++);
        for (int i = 0; i < columnsCount; i++) {
            Cell registrationCell = registrationRow.createCell(i);
            registrationCell.setCellStyle(importantCellStyle);

            if (i == 0) {
                registrationCell.setCellValue(time.format(DateTimeFormatter.ofPattern("H:mm")));
                time = time.plusHours(1);
            }

            if (i == 1) {
                registrationCell.setCellValue("Registration");
                sheet.addMergedRegion(new CellRangeAddress(2, 2, i, columnsCount - 1));
            }
        }

        int totalStagesCount = columnsCount - MAIN_COLUMNS_COUNT;
        int premiershipGroupNumber = 0;
        String previousLevel = Level.BEGINNER;
        for (Map.Entry<Dance, List<Category>> categoriesOfDanceEntry : categories.entrySet()) {
            Dance dance = categoriesOfDanceEntry.getKey();
            String level = ((SoloDance) dance).getLevel();

            if (!level.equals(previousLevel) && !previousLevel.equals(Level.INTERMEDIATE)) {
                Row resultsRow = sheet.createRow(rowIndex);
                for (int i = 0; i < columnsCount; i++) {
                    Cell resultsCell = resultsRow.createCell(i);
                    resultsCell.setCellStyle(importantCellStyle);

                    if (i == 0) {
                        time = time.plusMinutes(5); // For preparation
                        resultsCell.setCellValue(time.format(DateTimeFormatter.ofPattern("H:mm")));

                        String previousLevelCopy = previousLevel;
                        int categoriesOfLevelCount = (int) categories.values()
                                .stream()
                                .flatMap(Collection::stream)
                                .filter(category -> ((SoloDance) category.getDance()).getLevel().equals(previousLevelCopy))
                                .count();
                        int MINUTES_FOR_RESULTS = (int) (Math.ceil((double) categoriesOfLevelCount / 5) * 5) + 5;
                        time = time.plusMinutes(MINUTES_FOR_RESULTS);
                    }

                    if (i == 1) {
                        resultsCell.setCellValue("Results");
                        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex++, i, columnsCount - 1));
                    }
                }
            }

            List<Category> categoriesOfDance = categoriesOfDanceEntry.getValue();
            int maxCategoriesCountPerStage;
            String danceName = dance.getName();
            if (dance.isCupDance()) {
                maxCategoriesCountPerStage = getPremiershipGroups(categoriesOfDance).size();
            } else {
                maxCategoriesCountPerStage = (int) Math.ceil((double) categoriesOfDance.size() / totalStagesCount);
            }

            if (maxCategoriesCountPerStage > 1) {
                sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + maxCategoriesCountPerStage - 1, 0, 0));
                sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + maxCategoriesCountPerStage - 1, 1, 1));
            }

            int stageNumber = 0;
            int categoryNumber = 0;
            for (int i = 0; i < maxCategoriesCountPerStage; i++) {
                Row row = sheet.createRow(rowIndex++);
                for (int j = 0; j < columnsCount; j++) {
                    Cell cell = row.createCell(j);
                    cell.setCellStyle(standardCellStyle);

                    switch (j) {
                        case 0: {
                            cell.setCellValue(time.format(DateTimeFormatter.ofPattern("H:mm")));
                            break;
                        }
                        case 1: {
                            if (danceName.equals(DanceName.PRELIMINARY) || danceName.equals(DanceName.CHAMPIONSHIP)) {
                                cell.setCellValue(danceName);
                            } else {
                                cell.setCellValue(level + " " + danceName);
                            }
                            break;
                        }
                        case 2: {
                            int MINUTES_PER_PREMIERSHIP = 10;
                            int MINUTES_PER_CHAMPIONSHIP = 20;
                            if (dance.isCupDance()) {
                                List<List<String>> groups = getPremiershipGroups(categoriesOfDance);
                                List<String> group = groups.get(premiershipGroupNumber++);
                                cell.setCellValue(String.join(", ", group));
                                if (danceName.equals(DanceName.CHAMPIONSHIP)) {
                                    time = time.plusMinutes(MINUTES_PER_CHAMPIONSHIP * group.size());
                                } else {
                                    time = time.plusMinutes(MINUTES_PER_PREMIERSHIP * group.size());
                                }

                                if (halls.get(0).getStagesCount() > 1) {
                                    sheet.addMergedRegion(new CellRangeAddress(
                                            rowIndex - 1,
                                            rowIndex - 1,
                                            j,
                                            j + halls.get(0).getStagesCount() - 1
                                    ));
                                }
                                break;
                            }
                        }
                        default: {
                            int MINUTES_PER_CATEGORY = 5;

                            if (dance.isCupDance()) {
                                continue;
                            }

                            if (categoryNumber + stageNumber < categoriesOfDance.size()) {
                                Category category = categoriesOfDance.get(categoryNumber + stageNumber);
                                cell.setCellValue(category.getAgeDesignation());
                                if (categoryNumber + ++stageNumber == categoriesOfDance.size()) {
                                    time = time.plusMinutes(MINUTES_PER_CATEGORY * maxCategoriesCountPerStage);
                                }
                            }
                        }
                    }
                }
                stageNumber = 0;
                categoryNumber += totalStagesCount;
            }
            premiershipGroupNumber = 0;
            previousLevel = level;
        }

        Row resultsRow = sheet.createRow(rowIndex);
        for (int i = 0; i < columnsCount; i++) {
            Cell resultsCell = resultsRow.createCell(i);
            resultsCell.setCellStyle(importantCellStyle);

            if (i == 0) {
                resultsCell.setCellValue(time.format(DateTimeFormatter.ofPattern("H:mm")));
            }

            if (i == 1) {
                resultsCell.setCellValue("Results");
                sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex++, i, columnsCount - 1));
            }
        }


        // File
        Category category = categories.values().stream().findFirst().get().get(0);
        String timetableName = "timetable " + category.getFeis().getName() +
                ".xlsx";
        File timetableFile = null;
        try {
            timetableFile = createFile(timetableName);
            FileOutputStream outputStream = new FileOutputStream(timetableFile);
            book.write(outputStream);

            outputStream.close();
            book.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download/" + Objects.requireNonNull(timetableFile).getName();
    }

    private File createFile(String fileName) throws IOException {
        final String DIRECTORY_NAME = "D:/files";
        File directory = new File(DIRECTORY_NAME);
        directory.mkdir();

        File reportFile = new File(directory, fileName);
        reportFile.createNewFile();
        return reportFile;
    }

    private List<List<String>> getPremiershipGroups(List<Category> categories) {
        List<List<String>> ageDesignations = new ArrayList<>();
        List<Category> categoriesCopy = new ArrayList<>(categories);
        List<String> group = new ArrayList<>();

        if (categoriesCopy.size() == 1) {
            group.add(categoriesCopy.get(0).getAgeDesignation());
            ageDesignations.add(group);
            return ageDesignations;
        }

        final int STANDARD_GROUP_SIZE = 3;
        int nonStandardGroupsCount = categoriesCopy.size() % STANDARD_GROUP_SIZE;
        for (int i = 1; i <= nonStandardGroupsCount; i++) {
            group.add(categoriesCopy.get(0).getAgeDesignation());
            group.add(categoriesCopy.get(1).getAgeDesignation());
            ageDesignations.add(new ArrayList<>(group));
            categoriesCopy.remove(0);
            categoriesCopy.remove(0);
            if (categoriesCopy.isEmpty()) return ageDesignations;
            group = new ArrayList<>();
        }

        for (int i = 0; i < categoriesCopy.size(); i += 3) {
            group.add(categoriesCopy.get(i).getAgeDesignation());
            group.add(categoriesCopy.get(i + 1).getAgeDesignation());
            if (i + 2 < categoriesCopy.size()) {
                group.add(categoriesCopy.get(i + 2).getAgeDesignation());
            }
            ageDesignations.add(new ArrayList<>(group));
            group = new ArrayList<>();
        }

        return ageDesignations;
    }

    private CellStyle getStandardCellStyle() {
        CellStyle style = book.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);

        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());

        return style;
    }

    private CellStyle makeImportantCellStyle(CellStyle style) {
        Font font = book.createFont();
        font.setBold(true);

        CellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(style);
        newStyle.setFont(font);
        return newStyle;
    }

    private int getColumnsCount(List<Hall> halls) {
        int columnsCount = 2;
        for (Hall hall : halls) {
            columnsCount += hall.getStagesCount();
        }
        return columnsCount;
    }
}
