package it.elizc.feisplanner.core.repository.userrepository;

import it.elizc.feisplanner.core.model.User;
import org.springframework.dao.DataAccessException;

public interface UserRepository {
    User addUser(String email, String password, String firstName, String surname, String schoolName)
            throws DataAccessException;

    User getUserByEmail(String email);
}
