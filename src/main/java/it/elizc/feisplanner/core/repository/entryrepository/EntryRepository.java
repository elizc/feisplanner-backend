package it.elizc.feisplanner.core.repository.entryrepository;

import it.elizc.feisplanner.core.model.Entry;
import it.elizc.feisplanner.web.request.AddSoloDanceRequest;

import java.util.List;

public interface EntryRepository {
    List<Entry> getEntriesByUserId(int userId);

    List<Entry> getEntriesByFeisName(String feisName);

    Entry addEntry(String feisId, String studentId, String creationDate, List<AddSoloDanceRequest> soloDances);

    Entry editEntry(int id, int feisId, int studentId, List<AddSoloDanceRequest> soloDances);

    void deleteEntry(int id);

    void setNumbers(List<Entry> entries);

    boolean numbersAreSet(String feisName);
}
