package it.elizc.feisplanner.core.repository.entryrepository;

import it.elizc.feisplanner.core.model.*;
import it.elizc.feisplanner.web.request.AddSoloDanceRequest;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DbEntryRepository implements EntryRepository {
    private JdbcTemplate jdbc;
    private GeneratedKeyHolder keyHolder;
    private final Comparator<Dance> compareByDanceName = (dance1, dance2) -> {
        Map<String, Integer> namesOrder = new HashMap<>();
        namesOrder.put(DanceName.REEL, 1);
        namesOrder.put(DanceName.LIGHT, 2);
        namesOrder.put(DanceName.SLIP, 3);
        namesOrder.put(DanceName.SINGLE, 4);
        namesOrder.put(DanceName.TREBLE, 5);
        namesOrder.put(DanceName.HORNPIPE, 6);
        namesOrder.put(DanceName.TRAD_SET, 7);
        namesOrder.put(DanceName.PREMIERSHIP, 8);
        namesOrder.put(DanceName.PRELIMINARY, 9);
        namesOrder.put(DanceName.CHAMPIONSHIP, 10);

        String danceName1 = dance1.getName();
        Integer orderNumber1 = namesOrder.get(danceName1);

        String danceName2 = dance2.getName();
        Integer orderNumber2 = namesOrder.get(danceName2);

        return orderNumber1.compareTo(orderNumber2);
    };

    private final Comparator<EntryItem> compareByLevel = (entryItem1, entryItem2) -> {
        Map<String, Integer> levelsOrder = new HashMap<>();
        levelsOrder.put(Level.BEGINNER, 1);
        levelsOrder.put(Level.PRIMARY, 2);
        levelsOrder.put(Level.INTERMEDIATE, 3);
        levelsOrder.put(Level.OPEN, 4);

        SoloDance dance1 = (SoloDance) entryItem1.getDance();
        String level1 = dance1.getLevel();
        Integer orderNumber1 = levelsOrder.get(level1);

        SoloDance dance2 = (SoloDance) entryItem2.getDance();
        String level2 = dance2.getLevel();
        Integer orderNumber2 = levelsOrder.get(level2);

        return orderNumber1.compareTo(orderNumber2);
    };

    public DbEntryRepository(JdbcTemplate jdbcTemplate, GeneratedKeyHolder keyHolder) {
        this.jdbc = jdbcTemplate;
        this.keyHolder = keyHolder;
    }

    @Override
    public List<Entry> getEntriesByUserId(int userId) {
        List<Student> students = getStudentsByUser(userId);
        Function<Student, Stream<Entry>> studentToEntries = (student) -> {
            String sql = "SELECT * FROM entries WHERE student_id = ?";
            RowMapper<Entry> mapper = (resultSet, i) -> {
                int id = resultSet.getInt("id");
                String creationDate = resultSet.getString("entry_date");
                int feisId = resultSet.getInt("feis_id");

                List<EntryItem> entryItems = getEntryItemByEntryId(id)
                        .stream()
                        .sorted(Comparator
                                .comparing(EntryItem::getDance, compareByDanceName)
                                .thenComparing(compareByLevel))
                        .collect(Collectors.toList());
                Feis feis = getFeisById(feisId);
                return new Entry(Integer.toString(id), feis, student, entryItems);
            };
            return jdbc.query(sql, mapper, Integer.parseInt(student.getId())).stream();
        };
        return students
                .stream()
                .flatMap(studentToEntries)
                .collect(Collectors.toList());
    }

    @Override
    public List<Entry> getEntriesByFeisName(String feisName) {
        String sql = "SELECT * FROM entries WHERE feis_id = " +
                "(SELECT id FROM feises WHERE feis_name = ?)";
        RowMapper<Entry> mapper = (resultSet, i) -> {
            int id = resultSet.getInt("id");
            int feisId = resultSet.getInt("feis_id");
            int studentId = resultSet.getInt("student_id");
            int feisNumber = resultSet.getInt("entry_feis_number");

            List<EntryItem> entryItems = getEntryItemByEntryId(id)
                    .stream()
                    .sorted(Comparator
                            .comparing(EntryItem::getDance, compareByDanceName)
                            .thenComparing(compareByLevel))
                    .collect(Collectors.toList());

            Feis feis = getFeisById(feisId);
            Student student = getStudentById(studentId);
            return new Entry(Integer.toString(id), feisNumber, feis, student, entryItems);
        };

        return jdbc.query(sql, mapper, feisName);
    }

    @Override
    public Entry addEntry(String feisId, String studentId, String creationDate,
                          List<AddSoloDanceRequest> soloDances) throws DataAccessException {
        String sql = "INSERT INTO entries (entry_date, student_id, feis_id) VALUES (?, ?, ?)";
        PreparedStatementCreator insertEntry = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, creationDate);
            statement.setInt(2, Integer.parseInt(studentId));
            statement.setInt(3, Integer.parseInt(feisId));
            return statement;
        };
        jdbc.update(insertEntry, keyHolder);

        Integer entryId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        List<EntryItem> entryItems = new ArrayList<>();
        soloDances.forEach(soloDance -> {
            EntryItem entryItem = addEntryItem(entryId, soloDance);
            entryItems.add(entryItem);
        });

        Feis feis = getFeisById(Integer.parseInt(feisId));
        Student student = getStudentById(Integer.parseInt(studentId));

        return new Entry(Integer.toString(entryId), feis, student, entryItems);
    }

    @Override
    public Entry editEntry(int id, int feisId, int studentId, List<AddSoloDanceRequest> soloDances) {
        deleteEntryItems(id);

        List<EntryItem> entryItems = new ArrayList<>();
        soloDances.forEach(soloDance -> {
            EntryItem entryItem = addEntryItem(id, soloDance);
            entryItems.add(entryItem);
        });

        Feis feis = getFeisById(feisId);
        Student student = getStudentById(studentId);

        return new Entry(Integer.toString(id), feis, student, entryItems);
    }

    @Override
    public void deleteEntry(int id) {
        deleteEntryItems(id);

        String deleteEntrySql = "DELETE FROM entries WHERE id = ?";
        PreparedStatementCreator deleteEntry = connection -> {
            PreparedStatement statement = connection.prepareStatement(deleteEntrySql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, id);
            return statement;
        };
        jdbc.update(deleteEntry, keyHolder);
    }

    @Override
    public void setNumbers(List<Entry> entries) {
        String updateEntrySql = "UPDATE entries SET entry_feis_number = ? WHERE id = ?";
        for (Entry entry : entries) {
            PreparedStatementCreator setNumber = connection -> {
                PreparedStatement statement = connection.prepareStatement(updateEntrySql, Statement.RETURN_GENERATED_KEYS);
                statement.setInt(1, entry.getFeisNumber());
                statement.setInt(2, Integer.parseInt(entry.getId()));
                return statement;
            };

            jdbc.update(setNumber, keyHolder);
        }

        int feisId = Integer.parseInt(entries.get(0).getFeis().getId());
        String updateFeisSql = "UPDATE feises SET feis_numbers_are_set = TRUE WHERE id = ?";
        PreparedStatementCreator setFeisState = connection -> {
            PreparedStatement statement = connection.prepareStatement(updateFeisSql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, feisId);
            return statement;
        };

        jdbc.update(setFeisState, keyHolder);
    }

    @Override
    public boolean numbersAreSet(String feisName) {
        String sql = "SELECT feis_numbers_are_set from feises WHERE feis_name = ?";
        RowMapper<Boolean> mapper = (resultSet, i) -> resultSet.getBoolean("feis_numbers_are_set");
        return jdbc.query(sql, mapper, feisName).get(0);
    }

    private void deleteEntryItems(int entryId) {
        String deleteItemSql = "DELETE FROM entry_items WHERE entry_id = ?";
        PreparedStatementCreator deleteItem = connection -> {
            PreparedStatement statement = connection.prepareStatement(deleteItemSql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, entryId);
            return statement;
        };
        jdbc.update(deleteItem, keyHolder);
    }

    private EntryItem addEntryItem(int entryId, AddSoloDanceRequest soloDance) {
        int soloDanceId = getSoloDanceId(soloDance);

        String sql = "INSERT INTO entry_items (entry_id, solo_dance_id) VALUES (?, ?)";
        PreparedStatementCreator insertEntryItem = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, entryId);
            statement.setInt(2, soloDanceId);
            return statement;
        };
        jdbc.update(insertEntryItem, keyHolder);

        Integer entryItemId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        Dance dance = new SoloDance(Integer.toString(entryItemId), soloDance.getDanceName(), soloDance.getLevel());
        return new EntryItem(Integer.toString(entryItemId), dance);
    }

    private int getSoloDanceId(AddSoloDanceRequest soloDance) {
        int soloDanceTypeId = getSoloDanceTypeId(soloDance.getDanceName());
        int levelId = getLevelId(soloDance.getLevel());

        String sql = "SELECT id from solo_dances WHERE level_id = ? AND solo_dance_type_id = ?";
        return jdbc.query(sql, (resultSet, i) -> resultSet.getInt("id"), levelId, soloDanceTypeId)
                .get(0);
    }

    private int getSoloDanceTypeId(String name) {
        String sql = "SELECT id FROM solo_dances_types WHERE solo_dance_type_name = ?";
        return jdbc.query(sql, (resultSet, i) -> resultSet.getInt("id"), name).get(0);
    }

    private int getLevelId(String name) {
        String sql = "SELECT id FROM levels WHERE level_name = ?";
        return jdbc.query(sql, (resultSet, i) -> resultSet.getInt("id"), name).get(0);
    }

    private Feis getFeisById(Integer id) {
        String sql = "SELECT feis_name, feis_type, feis_start_date, feis_end_date, school_name, " +
                "schools.id as school_id, school_region, school_city " +
                "FROM feises " +
                "INNER JOIN schools ON feises.school_id = schools.id " +
                "WHERE feises.id = ?";
        RowMapper<Feis> mapper = (resultSet, i) -> {
            String name = resultSet.getString("feis_name");
            String type = resultSet.getString("feis_type");
            String startDate = resultSet.getString("feis_start_date");
            String endDate = resultSet.getString("feis_end_date");
            int schoolId = resultSet.getInt("school_id");
            String schoolName = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");

            School school = new School(Integer.toString(schoolId), schoolName, region, city);
            return new Feis(Integer.toString(id), name, type, startDate, endDate, school);
        };

        return jdbc.query(sql, mapper, id).get(0);
    }

    public List<Student> getStudentsByUser(int userId) {
        User teacher = getUserById(userId);

        String sql = "SELECT * FROM students " +
                "WHERE user_id = ?" +
                "ORDER BY student_surname";
        RowMapper<Student> mapper = (resultSet, i) -> {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("student_first_name");
            String surname = resultSet.getString("student_surname");
            String birthDate = resultSet.getString("student_birth_date");
            String gender = resultSet.getString("student_gender");
            String reelLevel = resultSet.getString("student_reel_level");
            String lightLevel = resultSet.getString("student_light_level");
            String slipLevel = resultSet.getString("student_slip_level");
            String singleLevel = resultSet.getString("student_single_level");
            String trebleLevel = resultSet.getString("student_treble_level");
            String hornpipeLevel = resultSet.getString("student_hornpipe_level");
            String tradSetLevel = resultSet.getString("student_trad_set_level");
            int wonPrelimsCount = resultSet.getInt("student_won_prelims_count");

            return new Student(Integer.toString(id), firstName, surname, gender, birthDate,
                    reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                    tradSetLevel, wonPrelimsCount, teacher);
        };

        return jdbc.query(sql, mapper, userId);
    }

    private User getUserById(int userId) {
        String sql = "SELECT user_email, user_first_name, user_surname, " +
                "school_name, schools.id as school_id, school_region, school_city " +
                "FROM users " +
                "INNER JOIN schools ON users.school_id = schools.id " +
                "WHERE users.id = ?";

        RowMapper<User> mapper = (resultSet, i) -> {
            String email = resultSet.getString("user_email");
            String firstName = resultSet.getString("user_first_name");
            String surname = resultSet.getString("user_surname");
            int schoolId = resultSet.getInt("school_id");
            String schoolName = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");

            School school = new School(Integer.toString(schoolId), schoolName, region, city);
            return new User(Integer.toString(userId), email, firstName, surname, school);
        };

        return jdbc.query(sql, mapper, userId).get(0);
    }

    private Student getStudentById(int id) {
        String sql = "SELECT * FROM students WHERE id = ?";
        RowMapper<Student> mapper = (resultSet, i) -> {
            String firstName = resultSet.getString("student_first_name");
            String surname = resultSet.getString("student_surname");
            String birthDate = resultSet.getString("student_birth_date");
            String gender = resultSet.getString("student_gender");
            String reelLevel = resultSet.getString("student_reel_level");
            String lightLevel = resultSet.getString("student_light_level");
            String slipLevel = resultSet.getString("student_slip_level");
            String singleLevel = resultSet.getString("student_single_level");
            String trebleLevel = resultSet.getString("student_treble_level");
            String hornpipeLevel = resultSet.getString("student_hornpipe_level");
            String tradSetLevel = resultSet.getString("student_trad_set_level");
            int wonPrelimsCount = resultSet.getInt("student_won_prelims_count");
            int userId = resultSet.getInt("user_id");

            User teacher = getUserById(userId);
            return new Student(Integer.toString(id), firstName, surname, gender, birthDate,
                    reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                    tradSetLevel, wonPrelimsCount, teacher);
        };

        return jdbc.query(sql, mapper, id).get(0);
    }

    private School getSchoolByUserId(int userId) {
        String sql = "SELECT schools.id as school_id, school_name, school_city, school_region " +
                "FROM schools " +
                "INNER JOIN users ON users.school_id = schools.id " +
                "WHERE users.school_id = ?";

        RowMapper<School> mapper = (resultSet, i) -> {
            String id = resultSet.getString("school_id");
            String name = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");

            return new School(id, name, region, city);
        };

        return jdbc.query(sql, mapper, userId).get(0);
    }

    private List<EntryItem> getEntryItemByEntryId(int entryId) {
        String sql = "SELECT * FROM entry_items WHERE entry_id = ?";
        RowMapper<EntryItem> rowMapper = (resultSet, i) -> {
            int id = resultSet.getInt("id");
            int soloDanceId = resultSet.getInt("solo_dance_id");

            Dance dance = getSoloDanceById(soloDanceId);
            return new EntryItem(Integer.toString(id), dance);
        };

        return jdbc.query(sql, rowMapper, entryId);
    }

    private SoloDance getSoloDanceById(int soloDanceId) {
        String sql = "SELECT solo_dance_type_name, level_name " +
                "FROM solo_dances " +
                "INNER JOIN solo_dances_types ON solo_dances_types.id = solo_dances.solo_dance_type_id " +
                "INNER JOIN levels ON levels.id = solo_dances.level_id " +
                "WHERE solo_dances.id = ?";

        RowMapper<SoloDance> rowMapper = (resultSet, i) -> {
            String level = resultSet.getString("level_name");
            String name = resultSet.getString("solo_dance_type_name");
            return new SoloDance(Integer.toString(soloDanceId), name, level);
        };

        return jdbc.query(sql, rowMapper, soloDanceId).get(0);
    }
}
