package it.elizc.feisplanner.core.repository.feisrepository;

import it.elizc.feisplanner.core.model.Feis;
import it.elizc.feisplanner.core.model.School;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;

public class DbFeisRepository implements FeisRepository {
    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;
    private RowMapper<Feis> mapper = (resultSet, i) -> {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("feis_name");
        String type = resultSet.getString("feis_type");
        String startDate = resultSet.getString("feis_start_date");
        String endDate = resultSet.getString("feis_end_date");
        int schoolId = resultSet.getInt("school_id");
        String schoolName = resultSet.getString("school_name");
        String region = resultSet.getString("school_region");
        String city = resultSet.getString("school_city");

        School school = new School(Integer.toString(schoolId), schoolName, region, city);
        return new Feis(Integer.toString(id), name, type, startDate, endDate, school);
    };

    public DbFeisRepository(JdbcTemplate jdbc, KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public List<Feis> getFeises() {
        String sql = "SELECT feises.id, feis_name, feis_type, feis_start_date, feis_end_date, school_name, " +
                "schools.id as school_id, school_region, school_city " +
                "FROM feises " +
                "INNER JOIN schools ON feises.school_id = schools.id";
        return jdbc.query(sql, mapper);
    }

    @Override
    public List<Feis> getFeisesByUser(int userId) {
        String sql = "SELECT feises.id, feis_name, feis_type, feis_start_date, feis_end_date, school_name, " +
                "schools.id as school_id, school_region, school_city " +
                "FROM feises " +
                "INNER JOIN schools ON feises.school_id = schools.id " +
                "WHERE school_id = (SELECT school_id FROM users WHERE id = ?)";
        return jdbc.query(sql, mapper, userId);
    }

    @Override
    public Feis addFeis(String name, String type, String startDate, String endDate, String schoolName)
            throws DataAccessException {
        String sql = "INSERT INTO feises (feis_name, feis_type, feis_start_date, feis_end_date, school_id) " +
                "VALUES (?, ?, ?, ?, ?)";

        School school = getSchoolByName(schoolName);
        PreparedStatementCreator insertFeis = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);
            statement.setString(2, type);
            statement.setString(3, startDate);
            statement.setString(4, endDate);
            statement.setInt(5, Integer.parseInt(school.getId()));
            return statement;
        };
        jdbc.update(insertFeis, keyHolder);

        Integer feisId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        return new Feis(Integer.toString(feisId), name, type, startDate, endDate, school);
    }

    @Override
    public Feis editFeis(String id, String name, String type, String startDate, String endDate, String schoolName)
            throws DataAccessException {
        String sql = "UPDATE feises SET feis_name = ?, feis_type = ?, " +
                "feis_start_date = ?, feis_end_date = ?, school_id = ? " +
                "WHERE id = ?";

        School school = getSchoolByName(schoolName);
        PreparedStatementCreator updateFeis = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);
            statement.setString(2, type);
            statement.setString(3, startDate);
            statement.setString(4, endDate);
            statement.setInt(5, Integer.parseInt(school.getId()));
            statement.setInt(6, Integer.parseInt(id));
            return statement;
        };
        jdbc.update(updateFeis, keyHolder);

        return new Feis(id, name, type, startDate, endDate, school);
    }

    @Override
    public Feis deleteFeis(Integer id) {
        Feis deletedFeis = getFeisById(id);
        String sql = "DELETE FROM feises WHERE id = ?";
        PreparedStatementCreator deleteFeis = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, id);
            return statement;
        };
        jdbc.update(deleteFeis, keyHolder);
        return deletedFeis;
    }

    private Feis getFeisById(Integer id) {
        String sql = "SELECT feis_name, feis_type, feis_start_date, feis_end_date, school_name, " +
                "schools.id as school_id, school_region, school_city " +
                "FROM feises " +
                "INNER JOIN schools ON feises.school_id = schools.id " +
                "WHERE feises.id = ?";
        RowMapper<Feis> mapper = (resultSet, i) -> {
            String name = resultSet.getString("feis_name");
            String type = resultSet.getString("feis_type");
            String startDate = resultSet.getString("feis_start_date");
            String endDate = resultSet.getString("feis_end_date");
            int schoolId = resultSet.getInt("school_id");
            String schoolName = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");

            School school = new School(Integer.toString(schoolId), schoolName, region, city);
            return new Feis(Integer.toString(id), name, type, startDate, endDate, school);
        };

        return jdbc.query(sql, mapper, id).get(0);
    }

    private School getSchoolByName(String name) {
        String sqlGetSchool = "SELECT * FROM schools WHERE school_name = ?";

        return jdbc.query(sqlGetSchool, (resultSet, i) -> {
            int id = resultSet.getInt("id");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");
            return new School(Integer.toString(id), name, region, city);
        }, name).get(0);
    }
}
