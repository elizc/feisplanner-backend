package it.elizc.feisplanner.core.repository.schoolrepository;

import it.elizc.feisplanner.core.model.School;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;

public class DbSchoolRepository implements SchoolRepository {
    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    public DbSchoolRepository(JdbcTemplate jdbc, KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public School addSchool(String name, String region, String city) throws DataAccessException {
        String sql = "INSERT INTO schools (school_name, school_region, school_city) VALUES (?, ?, ?)";
        PreparedStatementCreator insertSchool = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);
            statement.setString(2, region);
            statement.setString(3, city);
            return statement;
        };
        jdbc.update(insertSchool, keyHolder);

        Integer schoolId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        return new School(Integer.toString(schoolId), name, region, city);
    }

    @Override
    public List<School> getSchools() {
        String sql = "SELECT * FROM schools " +
                "WHERE LOWER(school_name) != 'no school'" +
                "ORDER BY school_name";
        RowMapper<School> mapper = (resultSet, i) -> {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");
            return new School(Integer.toString(id), name, region, city);
        };

        return jdbc.query(sql, mapper);
    }

    @Override
    public List<String> getSchoolsNames() {
        String sql = "SELECT school_name FROM schools ORDER BY school_name";
        RowMapper<String> mapper = (resultSet, i) -> resultSet.getString("school_name");
        return jdbc.query(sql, mapper);
    }

    @Override
    public School editSchool(School editedSchool) throws DataAccessException {
        String sql = "UPDATE schools SET school_name = ?, school_region = ?, school_city = ? WHERE id = ?";
        PreparedStatementCreator updateSchool = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, editedSchool.getName());
            statement.setString(2, editedSchool.getRegion());
            statement.setString(3, editedSchool.getCity());
            statement.setInt(4, Integer.parseInt(editedSchool.getId()));
            return statement;
        };
        jdbc.update(updateSchool, keyHolder);

        return editedSchool;
    }

    @Override
    public School deleteSchool(Integer id) {
        School deletedSchool = getSchoolById(id);

        String sql = "DELETE FROM schools WHERE id = ?";
        PreparedStatementCreator deleteSchool = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, id);
            return statement;
        };
        jdbc.update(deleteSchool, keyHolder);

        return deletedSchool;
    }

    private School getSchoolById(Integer id) {
        String sql = "SELECT * FROM schools WHERE id = ?";
        RowMapper<School> mapper = (resultSet, i) -> {
            String name = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");
            return new School(Integer.toString(id), name, region, city);
        };

        return jdbc.query(sql, mapper, id).get(0);
    }
}
