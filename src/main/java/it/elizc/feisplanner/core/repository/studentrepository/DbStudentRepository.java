package it.elizc.feisplanner.core.repository.studentrepository;

import it.elizc.feisplanner.core.model.School;
import it.elizc.feisplanner.core.model.Student;
import it.elizc.feisplanner.core.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;

public class DbStudentRepository implements StudentRepository {
    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    public DbStudentRepository(JdbcTemplate jdbc, KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public List<Student> getStudentsByUser(int userId) {
        User teacher = getUserById(userId);

        String sql = "SELECT * FROM students " +
                "WHERE user_id = ?" +
                "ORDER BY student_surname";
        RowMapper<Student> mapper = (resultSet, i) -> {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("student_first_name");
            String surname = resultSet.getString("student_surname");
            String birthDate = resultSet.getString("student_birth_date");
            String gender = resultSet.getString("student_gender");
            String reelLevel = resultSet.getString("student_reel_level");
            String lightLevel = resultSet.getString("student_light_level");
            String slipLevel = resultSet.getString("student_slip_level");
            String singleLevel = resultSet.getString("student_single_level");
            String trebleLevel = resultSet.getString("student_treble_level");
            String hornpipeLevel = resultSet.getString("student_hornpipe_level");
            String tradSetLevel = resultSet.getString("student_trad_set_level");
            int wonPrelimsCount = resultSet.getInt("student_won_prelims_count");

            return new Student(Integer.toString(id), firstName, surname, gender, birthDate,
                    reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                    tradSetLevel, wonPrelimsCount, teacher);
        };

        return jdbc.query(sql, mapper, userId);
    }

    @Override
    public Student addStudent(String firstName, String surname, String gender, String birthDate,
                              String reelLevel, String lightLevel, String slipLevel, String singleLevel, String trebleLevel,
                              String hornpipeLevel, String tradSetLevel, int wonPrelimsCount, String userId) {
        String sql = "INSERT INTO students (student_first_name, student_surname, student_birth_date, student_gender, " +
                "student_reel_level, student_light_level, student_slip_level, student_single_level, " +
                "student_treble_level, student_hornpipe_level, student_trad_set_level, " +
                "student_won_prelims_count, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatementCreator insertSchool = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, firstName);
            statement.setString(2, surname);
            statement.setString(3, birthDate);
            statement.setString(4, gender);
            statement.setString(5, reelLevel);
            statement.setString(6, lightLevel);
            statement.setString(7, slipLevel);
            statement.setString(8, singleLevel);
            statement.setString(9, trebleLevel);
            statement.setString(10, hornpipeLevel);
            statement.setString(11, tradSetLevel);
            statement.setInt(12, wonPrelimsCount);
            statement.setInt(13, Integer.parseInt(userId));
            return statement;
        };
        jdbc.update(insertSchool, keyHolder);

        User teacher = getUserById(Integer.parseInt(userId));
        Integer studentId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        return new Student(Integer.toString(studentId), firstName, surname, gender, birthDate,
                reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                tradSetLevel, wonPrelimsCount, teacher);
    }

    @Override
    public Student editStudent(Student editedStudent) {
        String sql = "UPDATE students SET student_first_name = ?, student_surname = ?, " +
                "student_birth_date = ?, student_gender = ?, student_reel_level = ?, student_light_level = ?, " +
                "student_slip_level = ?, student_single_level = ?, student_treble_level = ?, " +
                "student_hornpipe_level = ?, student_trad_set_level = ?, student_won_prelims_count = ? " +
                "WHERE id = ?";
        PreparedStatementCreator updateStudent = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, editedStudent.getFirstName());
            statement.setString(2, editedStudent.getSurname());
            statement.setString(3, editedStudent.getBirthDate());
            statement.setString(4, editedStudent.getGender());
            statement.setString(5, editedStudent.getReelLevel());
            statement.setString(6, editedStudent.getLightLevel());
            statement.setString(7, editedStudent.getSlipLevel());
            statement.setString(8, editedStudent.getSingleLevel());
            statement.setString(9, editedStudent.getTrebleLevel());
            statement.setString(10, editedStudent.getHornpipeLevel());
            statement.setString(11, editedStudent.getTradSetLevel());
            statement.setInt(12, editedStudent.getWonPrelimsCount());
            statement.setInt(13, Integer.parseInt(editedStudent.getId()));
            return statement;
        };
        jdbc.update(updateStudent, keyHolder);

        return editedStudent;
    }

    @Override
    public Student deleteStudent(int studentId) {
        Student deletedStudent = getStudentById(studentId);

        String sql = "DELETE FROM students WHERE id = ?";
        PreparedStatementCreator deleteStudent = connection -> {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, studentId);
            return statement;
        };
        jdbc.update(deleteStudent, keyHolder);

        return deletedStudent;
    }

    private Student getStudentById(Integer id) {
        String sql = "SELECT * FROM students WHERE id = ?";
        RowMapper<Student> mapper = (resultSet, i) -> {
            String firstName = resultSet.getString("student_first_name");
            String surname = resultSet.getString("student_surname");
            String birthDate = resultSet.getString("student_birth_date");
            String gender = resultSet.getString("student_gender");
            String reelLevel = resultSet.getString("student_reel_level");
            String lightLevel = resultSet.getString("student_light_level");
            String slipLevel = resultSet.getString("student_slip_level");
            String singleLevel = resultSet.getString("student_single_level");
            String trebleLevel = resultSet.getString("student_treble_level");
            String hornpipeLevel = resultSet.getString("student_hornpipe_level");
            String tradSetLevel = resultSet.getString("student_trad_set_level");
            int wonPrelimsCount = resultSet.getInt("student_won_prelims_count");
            int userId = resultSet.getInt("user_id");

            User teacher = getUserById(userId);
            return new Student(Integer.toString(id), firstName, surname, gender, birthDate,
                    reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                    tradSetLevel, wonPrelimsCount, teacher);
        };

        return jdbc.query(sql, mapper, id).get(0);
    }

    private User getUserById(int userId) {
        String sql = "SELECT user_email, user_first_name, user_surname, " +
                "school_name, schools.id as school_id, school_region, school_city " +
                "FROM users " +
                "INNER JOIN schools ON users.school_id = schools.id " +
                "WHERE users.id = ?";

        RowMapper<User> mapper = (resultSet, i) -> {
            String email = resultSet.getString("user_email");
            String firstName = resultSet.getString("user_first_name");
            String surname = resultSet.getString("user_surname");
            int schoolId = resultSet.getInt("school_id");
            String schoolName = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");

            School school = new School(Integer.toString(schoolId), schoolName, region, city);
            return new User(Integer.toString(userId), email, firstName, surname, school);
        };

        return jdbc.query(sql, mapper, userId).get(0);
    }
}
