package it.elizc.feisplanner.core.repository.schoolrepository;

import it.elizc.feisplanner.core.model.School;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface SchoolRepository {
    School addSchool(String name, String region, String city) throws DataAccessException;

    List<School> getSchools();

    List<String> getSchoolsNames();

    School editSchool(School editedSchool) throws DataAccessException;

    School deleteSchool(Integer id);
}
