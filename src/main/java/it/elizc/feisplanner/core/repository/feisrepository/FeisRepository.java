package it.elizc.feisplanner.core.repository.feisrepository;

import it.elizc.feisplanner.core.model.Feis;

import java.util.List;

public interface FeisRepository {
    List<Feis> getFeises();

    List<Feis> getFeisesByUser(int userId);

    Feis addFeis(String name, String type, String startDate, String endDate, String schoolName);

    Feis editFeis(String id, String name, String type, String startDate, String endDate, String schoolName);

    Feis deleteFeis(Integer id);
}
