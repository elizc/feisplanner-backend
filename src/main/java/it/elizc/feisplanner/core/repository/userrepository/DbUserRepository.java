package it.elizc.feisplanner.core.repository.userrepository;

import it.elizc.feisplanner.core.model.School;
import it.elizc.feisplanner.core.model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Objects;

public class DbUserRepository implements UserRepository {
    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    public DbUserRepository(JdbcTemplate jdbc, KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public User addUser(String email, String password, String firstName, String surname, String schoolName)
            throws DataAccessException {
        String sqlInsertUser = "INSERT INTO users (user_email, user_password, user_first_name, user_surname, role_id, school_id) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        String sqlGetRoleId = "SELECT id FROM roles WHERE role_name = 'user'";

        School school = getSchoolByName(schoolName);
        Integer roleId = jdbc.queryForObject(sqlGetRoleId, (resultSet, i) -> resultSet.getInt("id"));
        PreparedStatementCreator insertUser = connection -> {
            PreparedStatement ps = connection.prepareStatement(sqlInsertUser, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setString(3, firstName);
            ps.setString(4, surname);
            ps.setInt(5, Objects.requireNonNull(roleId));
            ps.setInt(6, Integer.parseInt(school.getId()));
            return ps;
        };
        jdbc.update(insertUser, keyHolder);

        Integer userId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        return new User(userId.toString(), email, password, firstName, surname, "user", school);
    }

    @Override
    public User getUserByEmail(String email) {
        String sql = "SELECT users.id, user_password, user_first_name, user_surname, " +
                "school_name, schools.id as school_id, school_region, school_city, role_name " +
                "FROM users " +
                "INNER JOIN roles ON users.role_id = roles.id " +
                "INNER JOIN schools ON users.school_id = schools.id " +
                "WHERE user_email = ?";

        RowMapper<User> mapper = (resultSet, i) -> {
            int userId = resultSet.getInt("id");
            String password = resultSet.getString("user_password");
            String firstName = resultSet.getString("user_first_name");
            String surname = resultSet.getString("user_surname");
            String role = resultSet.getString("role_name");
            int schoolId = resultSet.getInt("school_id");
            String schoolName = resultSet.getString("school_name");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");

            School school = new School(Integer.toString(schoolId), schoolName, region, city);
            return new User(Integer.toString(userId), email, password, firstName, surname, role, school);
        };

        return jdbc.query(sql, mapper, email).get(0);
    }

    private School getSchoolByName(String name) {
        String sqlGetSchool = "SELECT * FROM schools WHERE school_name = ?";

        return jdbc.query(sqlGetSchool, (resultSet, i) -> {
            int id = resultSet.getInt("id");
            String region = resultSet.getString("school_region");
            String city = resultSet.getString("school_city");
            return new School(Integer.toString(id), name, region, city);
        }, name).get(0);
    }
}
