package it.elizc.feisplanner.core.repository.studentrepository;

import it.elizc.feisplanner.core.model.Student;

import java.util.List;

public interface StudentRepository {
    List<Student> getStudentsByUser(int userId);

    Student addStudent(String firstName, String surname, String gender, String birthDate,
                       String reelLevel, String lightLevel, String slipLevel, String singleLevel, String trebleLevel,
                       String hornpipeLevel, String tradSetLevel, int wonPrelimsCount, String userId);

    Student editStudent(Student editedStudent);

    Student deleteStudent(int studentId);
}
