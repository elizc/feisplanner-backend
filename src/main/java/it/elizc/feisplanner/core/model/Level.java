package it.elizc.feisplanner.core.model;

public class Level {
    public static final String BEGINNER = "Beginner";
    public static final String PRIMARY = "Primary";
    public static final String INTERMEDIATE = "Intermediate";
    public static final String OPEN = "Open";
}
