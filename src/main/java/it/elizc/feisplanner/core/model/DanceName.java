package it.elizc.feisplanner.core.model;

public class DanceName {
    public static final String REEL = "Reel";
    public static final String LIGHT = "Light Jig";
    public static final String SLIP = "Slip Jig";
    public static final String SINGLE = "Single Jig";
    public static final String TREBLE = "Treble Jig";
    public static final String HORNPIPE = "Hornpipe";
    public static final String TRAD_SET = "Trad. Set";
    public static final String PREMIERSHIP = "Premiership";
    public static final String PRELIMINARY = "Preliminary";
    public static final String CHAMPIONSHIP = "Championship";
}
