package it.elizc.feisplanner.core.model;

import java.util.List;

public class SoloCategory extends Category {
    private List<Entry> entries;

    public SoloCategory(Feis feis, int minAge, int maxAge, SoloDance dance, List<Entry> entries) {
        super(feis, dance, minAge, maxAge);
        this.entries = entries;
    }

    public List<Entry> getEntries() {
        return entries;
    }
}
