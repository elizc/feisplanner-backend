package it.elizc.feisplanner.core.model;

import java.util.List;

public class Entry {
    private String id;
    private int feisNumber;
    private String creationDate;
    private Feis feis;
    private Student student;
    private List<EntryItem> entryItems;

    public Entry(String id, Feis feis, Student student, List<EntryItem> entryItems) {
        this.id = id;
        this.feis = feis;
        this.student = student;
        this.entryItems = entryItems;
    }

    public Entry(String id, int feisNumber, Feis feis, Student student, List<EntryItem> entryItems) {
        this.id = id;
        this.feisNumber = feisNumber;
        this.feis = feis;
        this.student = student;
        this.entryItems = entryItems;
    }

    public String getId() {
        return id;
    }

    public int getFeisNumber() {
        return feisNumber;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public Feis getFeis() {
        return feis;
    }

    public Student getStudent() {
        return student;
    }

    public List<EntryItem> getEntryItems() {
        return entryItems;
    }

    public void setFeisNumber(int feisNumber) {
        this.feisNumber = feisNumber;
    }
}
