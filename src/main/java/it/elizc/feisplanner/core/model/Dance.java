package it.elizc.feisplanner.core.model;

import java.util.Objects;

public class Dance {
    private String id;
    private String name;

    public Dance(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public boolean isCupDance() {
        return name.equals(DanceName.PREMIERSHIP) ||
                name.equals(DanceName.PRELIMINARY) ||
                name.equals(DanceName.CHAMPIONSHIP);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dance)) return false;
        Dance dance = (Dance) o;
        return id.equals(dance.id) &&
                name.equals(dance.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
