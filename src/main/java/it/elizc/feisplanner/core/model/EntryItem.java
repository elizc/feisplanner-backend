package it.elizc.feisplanner.core.model;

public class EntryItem {
    private String id;
    private Dance dance;

    public EntryItem(String id, Dance dance) {
        this.id = id;
        this.dance = dance;
    }

    public String getId() {
        return id;
    }

    public Dance getDance() {
        return dance;
    }
}
