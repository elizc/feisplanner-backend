package it.elizc.feisplanner.core.model;

public class User {
    private String id;
    private String email;
    private String password;
    private String firstName;
    private String surname;
    private String role;
    private School school;

    public User(
            String id,
            String email,
            String password,
            String firstName,
            String surname,
            String role,
            School school) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.surname = surname;
        this.role = role;
        this.school = school;
    }

    public User(String id, String email, String firstName, String surname, School school) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.surname = surname;
        this.school = school;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getRole() {
        return role;
    }

    public School getSchool() {
        return school;
    }
}
