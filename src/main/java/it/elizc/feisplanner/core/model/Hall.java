package it.elizc.feisplanner.core.model;

public class Hall {
    private String name;
    private int stagesCount;

    public Hall(String name, int stagesCount) {
        this.name = name;
        this.stagesCount = stagesCount;
    }

    public String getName() {
        return name;
    }

    public int getStagesCount() {
        return stagesCount;
    }
}
