package it.elizc.feisplanner.core.model;

import java.util.Objects;

public class SoloDance extends Dance {
    private String level;

    public SoloDance(String id, String name, String level) {
        super(id, name);
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SoloDance)) return false;
        if (!super.equals(o)) return false;
        SoloDance soloDance = (SoloDance) o;
        return level.equals(soloDance.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), level);
    }
}
