package it.elizc.feisplanner.core.model;

public abstract class Category {
    private int id;
    private int minAge;
    private int maxAge;
    private Feis feis;
    private Dance dance;

    public Category(Feis feis, Dance dance, int minAge, int maxAge) {
        this.feis = feis;
        this.dance = dance;
        this.minAge = minAge;
        this.maxAge = maxAge;
    }

    public Dance getDance() {
        return dance;
    }

    public String getAgeDesignation() {
        if (minAge == 0 && maxAge == Integer.MAX_VALUE) {
            return "all ages";
        }
        return maxAge == Integer.MAX_VALUE ? "o" + minAge : "u" + (maxAge + 1);
    }

    public Feis getFeis() {
        return feis;
    }
}
