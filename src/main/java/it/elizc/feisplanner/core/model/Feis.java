package it.elizc.feisplanner.core.model;

public class Feis {
    private String id;
    private String name;
    private String type;
    private String startDate;
    private String endDate;
    private School school;

    public Feis(String id, String name, String type, String startDate, String endDate, School school) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.school = school;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public School getSchool() {
        return school;
    }
}
