package it.elizc.feisplanner.core.model;

public class School {
    private String id;
    private String name;
    private String region;
    private String city;

    public School(String id, String name, String region, String city) {
        this.id = id;
        this.name = name;
        this.region = region;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }
}
