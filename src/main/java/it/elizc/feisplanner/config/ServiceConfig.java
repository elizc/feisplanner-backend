package it.elizc.feisplanner.config;

import it.elizc.feisplanner.core.repository.entryrepository.EntryRepository;
import it.elizc.feisplanner.core.repository.feisrepository.FeisRepository;
import it.elizc.feisplanner.core.repository.schoolrepository.SchoolRepository;
import it.elizc.feisplanner.core.repository.studentrepository.StudentRepository;
import it.elizc.feisplanner.core.repository.userrepository.UserRepository;
import it.elizc.feisplanner.core.service.excelservice.ExcelService;
import it.elizc.feisplanner.core.service.excelservice.ExcelServiceImpl;
import it.elizc.feisplanner.core.service.entryservice.EntryService;
import it.elizc.feisplanner.core.service.entryservice.EntryServiceImpl;
import it.elizc.feisplanner.core.service.feisservice.FeisService;
import it.elizc.feisplanner.core.service.feisservice.FeisServiceImpl;
import it.elizc.feisplanner.core.service.schoolservice.SchoolService;
import it.elizc.feisplanner.core.service.schoolservice.SchoolServiceImpl;
import it.elizc.feisplanner.core.service.studentservice.StudentService;
import it.elizc.feisplanner.core.service.studentservice.StudentServiceImpl;
import it.elizc.feisplanner.core.service.categoryservice.CategoryService;
import it.elizc.feisplanner.core.service.categoryservice.CategoryServiceImpl;
import it.elizc.feisplanner.core.service.timatableservice.TimetableService;
import it.elizc.feisplanner.core.service.timatableservice.TimetableServiceImpl;
import it.elizc.feisplanner.core.service.userservice.UserService;
import it.elizc.feisplanner.core.service.userservice.UserServiceImpl;
import it.elizc.feisplanner.core.service.wordservice.WordService;
import it.elizc.feisplanner.core.service.wordservice.WordServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ServiceConfig {

    @Bean
    public SchoolService getSchoolService(final SchoolRepository schoolRepository) {
        return new SchoolServiceImpl(schoolRepository);
    }

    @Bean
    @Qualifier("userService")
    public UserService getUserService(final UserRepository userRepository, final PasswordEncoder encoder) {
        return new UserServiceImpl(userRepository, encoder);
    }

    @Bean
    public StudentService getStudentService(final StudentRepository studentRepository) {
        return new StudentServiceImpl(studentRepository);
    }

    @Bean
    public FeisService getFeisService(final FeisRepository feisRepository) {
        return new FeisServiceImpl(feisRepository);
    }

    @Bean
    public EntryService getEntryService(final EntryRepository entryRepository) {
        return new EntryServiceImpl(entryRepository);
    }

    @Bean
    public CategoryService getCategoryService(final EntryRepository entryRepository) {
        return new CategoryServiceImpl(entryRepository);
    }
    
    @Bean
    public ExcelService getExcelService() {
        return new ExcelServiceImpl();
    }

    @Bean
    public WordService getWordService() {
        return new WordServiceImpl();
    }

    @Bean
    public TimetableService getTimetableService() {
        return new TimetableServiceImpl();
    }
}
