package it.elizc.feisplanner.config;

import it.elizc.feisplanner.core.repository.entryrepository.DbEntryRepository;
import it.elizc.feisplanner.core.repository.entryrepository.EntryRepository;
import it.elizc.feisplanner.core.repository.feisrepository.DbFeisRepository;
import it.elizc.feisplanner.core.repository.feisrepository.FeisRepository;
import it.elizc.feisplanner.core.repository.schoolrepository.DbSchoolRepository;
import it.elizc.feisplanner.core.repository.schoolrepository.SchoolRepository;
import it.elizc.feisplanner.core.repository.studentrepository.StudentRepository;
import it.elizc.feisplanner.core.repository.studentrepository.DbStudentRepository;
import it.elizc.feisplanner.core.repository.userrepository.DbUserRepository;
import it.elizc.feisplanner.core.repository.userrepository.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

@Configuration
public class RepositoryConfig {

    @Bean
    public SchoolRepository schoolRepository (@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DbSchoolRepository(jdbcTemplate, new GeneratedKeyHolder());
    }

    @Bean
    public UserRepository userRepository (@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DbUserRepository(jdbcTemplate, new GeneratedKeyHolder());
    }

    @Bean
    public StudentRepository studentRepository (@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DbStudentRepository(jdbcTemplate, new GeneratedKeyHolder());
    }
    
    @Bean
    public FeisRepository feisRepository (@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DbFeisRepository(jdbcTemplate, new GeneratedKeyHolder());
    }

    @Bean
    public EntryRepository entryRepository (@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DbEntryRepository(jdbcTemplate, new GeneratedKeyHolder());
    }
}
