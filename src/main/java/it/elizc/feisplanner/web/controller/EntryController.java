package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.exception.EntryExistsException;
import it.elizc.feisplanner.core.exception.FeisExistsException;
import it.elizc.feisplanner.core.model.Entry;
import it.elizc.feisplanner.core.service.entryservice.EntryService;
import it.elizc.feisplanner.web.request.AddEntryRequest;
import it.elizc.feisplanner.web.request.AddSoloDanceRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/entries")
public class EntryController {
    private EntryService entryService;

    public EntryController(final EntryService entryService) {
        this.entryService = entryService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Entry>> getEntriesOfUser(@RequestParam(name = "user") String userId) {
        List<Entry> entries = entryService.getEntriesByUserId(userId);
        return ResponseEntity.status(HttpStatus.OK).body(entries);
    }

    @GetMapping("/feis")
    @ResponseBody
    public ResponseEntity<List<Entry>> getEntriesOfFeis(@RequestParam(name = "name") String feisName) {
        List<Entry> entries = entryService.getEntriesByFeisName(feisName);
        return ResponseEntity.status(HttpStatus.OK).body(entries);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Entry> addEntry(@RequestBody final AddEntryRequest request)
            throws EntryExistsException {
        String feisId = request.getFeisId();
        String studentId = request.getStudentId();
        List<AddSoloDanceRequest> soloDances = request.getSoloDances();

        Entry entry = entryService.addEntry(feisId, studentId, soloDances);
        return ResponseEntity.status(HttpStatus.CREATED).body(entry);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Entry> editEntry(@RequestBody final AddEntryRequest request, @PathVariable String id) {
        String feisId = request.getFeisId();
        String studentId = request.getStudentId();
        List<AddSoloDanceRequest> soloDances = request.getSoloDances();

        Entry editedEntry = entryService.editEntry(id, feisId, studentId, soloDances);
        return ResponseEntity.status(HttpStatus.OK).body(editedEntry);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteFeis(@PathVariable String id) {
        entryService.deleteEntry(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
