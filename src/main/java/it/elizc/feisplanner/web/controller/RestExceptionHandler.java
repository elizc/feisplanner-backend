package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.exception.EntryExistsException;
import it.elizc.feisplanner.core.exception.FeisExistsException;
import it.elizc.feisplanner.core.exception.SchoolExistsException;
import it.elizc.feisplanner.core.exception.UserExistsException;
import it.elizc.feisplanner.web.response.ApiErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(FeisExistsException.class)
    protected ResponseEntity<Object> handleExistingFeis(FeisExistsException ex) {
        final String message = "The feis already exists";
        ApiErrorResponse response = new ApiErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, message, ex);
        return buildResponseEntity(response);
    }

    @ExceptionHandler(SchoolExistsException.class)
    protected ResponseEntity<Object> handleExistingSchool(SchoolExistsException ex) {
        final String message = "The school already exists";
        ApiErrorResponse response = new ApiErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, message, ex);
        return buildResponseEntity(response);
    }

    @ExceptionHandler(UserExistsException.class)
    protected ResponseEntity<Object> handleExistingUser(UserExistsException ex) {
        final String message = "The user with this email already exists";
        ApiErrorResponse response = new ApiErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, message, ex);
        return buildResponseEntity(response);
    }

    @ExceptionHandler(EntryExistsException.class)
    protected ResponseEntity<Object> handleExistingEntry(EntryExistsException ex) {
        final String message = "The entry with these student and feis already exists";
        ApiErrorResponse response = new ApiErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, message, ex);
        return buildResponseEntity(response);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiErrorResponse response) {
        return ResponseEntity.status(response.getStatus()).body(response);
    }
}
