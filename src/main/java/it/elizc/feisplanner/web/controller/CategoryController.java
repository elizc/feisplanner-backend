package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.model.Category;
import it.elizc.feisplanner.core.service.categoryservice.CategoryService;
import it.elizc.feisplanner.core.service.entryservice.EntryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    private final CategoryService categoryService;
    private final EntryService entryService;

    public CategoryController(CategoryService categoryService, EntryService entryService) {
        this.categoryService = categoryService;
        this.entryService = entryService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Category>> getCategories(@RequestParam(name = "feis") String feisName) {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Category> categories = categoryService.categorize(feisName);
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }
}
