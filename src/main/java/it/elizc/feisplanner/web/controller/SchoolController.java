package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.exception.SchoolExistsException;
import it.elizc.feisplanner.core.model.School;
import it.elizc.feisplanner.core.service.schoolservice.SchoolService;
import it.elizc.feisplanner.web.request.AddSchoolRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/schools")
public class SchoolController {
    private SchoolService schoolService;

    public SchoolController(final SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<School> addSchool(@RequestBody final AddSchoolRequest request) throws SchoolExistsException {
        String schoolName = request.getName();
        String schoolRegion = request.getRegion();
        String schoolCity = request.getCity();
        School school = schoolService.addSchool(schoolName, schoolRegion, schoolCity);
        return ResponseEntity.status(HttpStatus.CREATED).body(school);
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<School>> getSchools() {
        List<School> schools = schoolService.getSchools();
        return ResponseEntity.status(HttpStatus.OK).body(schools);
    }

    @GetMapping(value = "/names")
    @ResponseBody
    public ResponseEntity<List<String>> getSchoolNames() {
        List<String> names = schoolService.getSchoolsNames();
        return ResponseEntity.status(HttpStatus.OK).body(names);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<School> editSchool(@RequestBody final School request) throws SchoolExistsException {
        School editedSchool = schoolService.editSchool(request);
        return ResponseEntity.status(HttpStatus.OK).body(editedSchool);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteSchool(@PathVariable String id) {
        schoolService.deleteSchool(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
