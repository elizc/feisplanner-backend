package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.exception.FeisExistsException;
import it.elizc.feisplanner.core.model.Feis;
import it.elizc.feisplanner.core.service.feisservice.FeisService;
import it.elizc.feisplanner.web.request.AddFeisRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/feises")
public class FeisController {
    private FeisService feisService;

    public FeisController(final FeisService feisService) {
        this.feisService = feisService;
    }

    @GetMapping("/unfinished")
    @ResponseBody
    public ResponseEntity<List<Feis>> getUnfinishedFeises() {
        List<Feis> feises = feisService.getUnfinishedFeises();
        return ResponseEntity.status(HttpStatus.OK).body(feises);
    }

    @GetMapping("/opened")
    @ResponseBody
    public ResponseEntity<List<Feis>> getOpenedFeises() {
        List<Feis> feises = feisService.getOpenedFeises();
        return ResponseEntity.status(HttpStatus.OK).body(feises);
    }

    @GetMapping()
    @ResponseBody
    public ResponseEntity<List<String>> getFeisNamesOfUser(@RequestParam(name = "user") String userId) {
        List<String> feises = feisService.getFeisNamesByUserId(userId);
        return ResponseEntity.status(HttpStatus.OK).body(feises);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Feis> addFeis(@RequestBody final AddFeisRequest request) throws FeisExistsException {
        String name = request.getName();
        String type = request.getType();
        String startDate = request.getStartDate();
        String endDate = request.getEndDate();
        String schoolName = request.getSchoolName();

        Feis feis = feisService.addFeis(name, type, startDate, endDate, schoolName);
        return ResponseEntity.status(HttpStatus.CREATED).body(feis);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Feis> editFeis(@RequestBody final AddFeisRequest request, @PathVariable String id)
            throws FeisExistsException {
        String name = request.getName();
        String type = request.getType();
        String startDate = request.getStartDate();
        String endDate = request.getEndDate();
        String schoolName = request.getSchoolName();

        Feis editedFeis = feisService.editFeis(id, name, type, startDate, endDate, schoolName);
        return ResponseEntity.status(HttpStatus.OK).body(editedFeis);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteFeis(@PathVariable String id) {
        feisService.deleteFeis(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
