package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.model.Category;
import it.elizc.feisplanner.core.model.Entry;
import it.elizc.feisplanner.core.service.categoryservice.CategoryService;
import it.elizc.feisplanner.core.service.excelservice.ExcelService;
import it.elizc.feisplanner.core.service.entryservice.EntryService;
import it.elizc.feisplanner.core.service.wordservice.WordService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/docs")
public class DocController {
    private final EntryService entryService;
    private final CategoryService categoryService;
    private final ExcelService excelService;
    private final WordService wordService;

    public DocController(
            final EntryService entryService,
            final CategoryService categoryService,
            final ExcelService excelService,
            final WordService wordService
    ) {
        this.entryService = entryService;
        this.categoryService = categoryService;
        this.excelService = excelService;
        this.wordService = wordService;
    }

    @GetMapping("/all-dancers")
    @ResponseBody
    public ResponseEntity<String> getDancersReport(@RequestParam(name = "feis") String feisName)
            throws IOException {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Entry> entries = entryService.getEntriesByFeisName(feisName);
        String reportLink = excelService.generateDancersReport(entries);
        return ResponseEntity.status(HttpStatus.OK).body(reportLink);
    }

    @GetMapping("/scoring-table")
    @ResponseBody
    public ResponseEntity<String> getScoringTable(@RequestParam(name = "feis") String feisName) {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Category> categories = categoryService.categorize(feisName);
        String docLink = excelService.generateScoringTable(categories);
        return ResponseEntity.status(HttpStatus.OK).body(docLink);
    }

    @GetMapping("/stage-lists")
    @ResponseBody
    public ResponseEntity<String> getStageLists(@RequestParam(name = "feis") String feisName) {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Category> categories = categoryService.categorize(feisName);
        String docLink = wordService.getStageLists(categories);
        return ResponseEntity.status(HttpStatus.OK).body(docLink);
    }

    @GetMapping("/numbers")
    @ResponseBody
    public ResponseEntity<String> getNumbers(@RequestParam(name = "feis") String feisName) {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Entry> entries = entryService.getEntriesByFeisName(feisName);
        String docLink = wordService.getNumbers(entries);
        return ResponseEntity.status(HttpStatus.OK).body(docLink);
    }

    @GetMapping("/stickers")
    @ResponseBody
    public ResponseEntity<String> getStickers(@RequestParam(name = "feis") String feisName) {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Entry> entries = entryService.getEntriesByFeisName(feisName);
        String docLink = wordService.getStickers(entries);
        return ResponseEntity.status(HttpStatus.OK).body(docLink);
    }
}
