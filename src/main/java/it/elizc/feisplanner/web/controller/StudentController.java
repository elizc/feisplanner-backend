package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.model.Student;
import it.elizc.feisplanner.core.service.studentservice.StudentService;
import it.elizc.feisplanner.web.request.AddStudentRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    private StudentService studentService;

    public StudentController(final StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Student>> getStudentsByTeacher(@RequestParam(name = "user") String userId) {
        List<Student> students = studentService.getStudentsByTeacher(userId);
        return ResponseEntity.status(HttpStatus.OK).body(students);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Student> addStudent(@RequestBody final AddStudentRequest request) {
        String firstName = request.getFirstName();
        String surname = request.getSurname();
        String birthDate = request.getBirthDate();
        String gender = request.getGender();
        String reelLevel = request.getReelLevel();
        String lightLevel = request.getLightLevel();
        String slipLevel = request.getSlipLevel();
        String singleLevel = request.getSingleLevel();
        String trebleLevel = request.getTrebleLevel();
        String hornpipeLevel = request.getHornpipeLevel();
        String tradSetLevel = request.getTradSetLevel();
        int wonPrelimsCount = request.getWonPrelimsCount();
        String userId = request.getUserId();

        Student student = studentService.addStudent(firstName, surname, gender, birthDate,
                reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel,
                tradSetLevel, wonPrelimsCount, userId);
        return ResponseEntity.status(HttpStatus.CREATED).body(student);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Student> editStudent(@RequestBody final Student request) {
        Student editedStudent = studentService.editStudent(request);
        return ResponseEntity.status(HttpStatus.OK).body(editedStudent);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteStudent(@PathVariable String id) {
        studentService.deleteStudent(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
