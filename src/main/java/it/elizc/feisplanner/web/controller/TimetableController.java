package it.elizc.feisplanner.web.controller;


import it.elizc.feisplanner.core.model.Category;
import it.elizc.feisplanner.core.model.Dance;
import it.elizc.feisplanner.core.model.Hall;
import it.elizc.feisplanner.core.service.categoryservice.CategoryService;
import it.elizc.feisplanner.core.service.entryservice.EntryService;
import it.elizc.feisplanner.core.service.timatableservice.TimetableService;
import it.elizc.feisplanner.web.request.CreateTimetableRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/timetable")
public class TimetableController {
    private final CategoryService categoryService;
    private final TimetableService timetableService;
    private final EntryService entryService;

    public TimetableController(
            final CategoryService categoryService,
            final TimetableService timetableService,
            final EntryService entryService) {
        this.categoryService = categoryService;
        this.timetableService = timetableService;
        this.entryService = entryService;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<String> createTimetable(
            @RequestBody CreateTimetableRequest request,
            @RequestParam(name = "feis") String feisName
    ) {
        if (!entryService.numbersAreSet(feisName)) {
            entryService.setNumbers(feisName);
        }

        List<Hall> halls = request.getHalls();
        int startHours = request.getStartHours();
        int startMinutes = request.getStartMinutes();
        List<Category> categories = categoryService.categorize(feisName);

        String link = timetableService.generateTimetable(convertToMap(categories), startHours, startMinutes, halls);
        return ResponseEntity.status(HttpStatus.OK).body(link);
    }

    private Map<Dance, List<Category>> convertToMap(List<Category> list) {
        Map<Dance, List<Category>> map = new LinkedHashMap<>();
        for (Category category : list) {
            Dance dance = category.getDance();

            if (!map.containsKey(dance)) {
                map.put(dance, new ArrayList<>());
            }

            List<Category> danceCategories = map.get(dance);
            danceCategories.add(category);
            map.replace(dance, danceCategories);
        }
        return map;
    }
}
