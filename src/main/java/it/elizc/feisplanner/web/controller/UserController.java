package it.elizc.feisplanner.web.controller;

import it.elizc.feisplanner.core.exception.UserExistsException;
import it.elizc.feisplanner.core.model.AuthToken;
import it.elizc.feisplanner.core.model.User;
import it.elizc.feisplanner.core.security.TokenProvider;
import it.elizc.feisplanner.core.service.userservice.UserDataService;
import it.elizc.feisplanner.web.request.SignInRequest;
import it.elizc.feisplanner.web.request.SignUpRequest;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private UserDataService userService;
    private AuthenticationManager authenticationManager;
    private TokenProvider jwtTokenUtil;

    public UserController(
            @Qualifier("userService") final UserDataService userService,
            final AuthenticationManager authenticationManager,
            final TokenProvider jwtTokenUtil
    ) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping(value = "/sign-up")
    public ResponseEntity<?> signUp(@RequestBody final SignUpRequest request) throws UserExistsException {
        User user = userService.addUser(
                request.getEmail(),
                request.getPassword(),
                request.getFirstName(),
                request.getSurname(),
                request.getSchoolName()
        );

        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(auth);
        String token = jwtTokenUtil.generateToken(auth, user);
        return ResponseEntity.status(HttpStatus.CREATED).body(new AuthToken(token));
    }

    @PostMapping(value = "/sign-in")
    public ResponseEntity<?> signIn(@RequestBody final SignInRequest signInUser) throws AuthenticationException {
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signInUser.getEmail(),
                        signInUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(auth);
        User user = userService.getUserByEmail(signInUser.getEmail());
        String token = jwtTokenUtil.generateToken(auth, user);
        return ResponseEntity.ok(new AuthToken(token));
    }

}
