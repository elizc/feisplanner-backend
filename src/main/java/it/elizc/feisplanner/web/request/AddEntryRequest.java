package it.elizc.feisplanner.web.request;

import java.util.List;

public class AddEntryRequest {
    private String feisId;
    private String studentId;
    private List<AddSoloDanceRequest> soloDances;

    public AddEntryRequest(String feisId, String studentId, List<AddSoloDanceRequest> soloDances) {
        this.feisId = feisId;
        this.studentId = studentId;
        this.soloDances = soloDances;
    }

    public String getFeisId() {
        return feisId;
    }

    public String getStudentId() {
        return studentId;
    }

    public List<AddSoloDanceRequest> getSoloDances() {
        return soloDances;
    }
}
