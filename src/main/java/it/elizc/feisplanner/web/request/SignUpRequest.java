package it.elizc.feisplanner.web.request;

public class SignUpRequest {
    private String email;
    private String password;
    private String firstName;
    private String surname;
    private String schoolName;

    public SignUpRequest(String email, String password, String firstName, String surname, String schoolName) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.surname = surname;
        this.schoolName = schoolName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getSchoolName() {
        return schoolName;
    }
}
