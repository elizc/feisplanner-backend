package it.elizc.feisplanner.web.request;

public class AddSoloDanceRequest {
    private String danceName;
    private String level;

    public AddSoloDanceRequest(String danceName, String level) {
        this.danceName = danceName;
        this.level = level;
    }

    public String getDanceName() {
        return danceName;
    }

    public String getLevel() {
        return level;
    }
}
