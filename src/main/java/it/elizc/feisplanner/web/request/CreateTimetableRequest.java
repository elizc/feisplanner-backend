package it.elizc.feisplanner.web.request;

import it.elizc.feisplanner.core.model.Hall;

import java.util.List;

public class CreateTimetableRequest {
    private int startHours;
    private int startMinutes;
    private List<Hall> halls;

    public CreateTimetableRequest(int startHours, int startMinutes, List<Hall> halls) {
        this.startHours = startHours;
        this.startMinutes = startMinutes;
        this.halls = halls;
    }

    public int getStartHours() {
        return startHours;
    }

    public int getStartMinutes() {
        return startMinutes;
    }

    public List<Hall> getHalls() {
        return halls;
    }
}
