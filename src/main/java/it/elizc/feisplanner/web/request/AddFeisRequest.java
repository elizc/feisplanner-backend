package it.elizc.feisplanner.web.request;

public class AddFeisRequest {
    private String name;
    private String type;
    private String startDate;
    private String endDate;
    private String schoolName;

    public AddFeisRequest(String name, String type, String startDate, String endDate, String schoolName) {
        this.name = name;
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.schoolName = schoolName;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getSchoolName() {
        return schoolName;
    }
}
