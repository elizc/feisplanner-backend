package it.elizc.feisplanner.web.request;

public class AddStudentRequest {
    private String firstName;
    private String surname;
    private String gender;
    private String birthDate;
    private String reelLevel;
    private String lightLevel;
    private String slipLevel;
    private String singleLevel;
    private String trebleLevel;
    private String hornpipeLevel;
    private String tradSetLevel;
    private int wonPrelimsCount;
    private String userId;

    public AddStudentRequest(String firstName, String surname, String gender, String birthDate,
                   String reelLevel, String lightLevel, String slipLevel, String singleLevel,
                   String trebleLevel, String hornpipeLevel, String tradSetLevel, int wonPrelimsCount, String userId) {
        this.firstName = firstName;
        this.surname = surname;
        this.gender = gender;
        this.birthDate = birthDate;
        this.reelLevel = reelLevel;
        this.lightLevel = lightLevel;
        this.slipLevel = slipLevel;
        this.singleLevel = singleLevel;
        this.trebleLevel = trebleLevel;
        this.hornpipeLevel = hornpipeLevel;
        this.tradSetLevel = tradSetLevel;
        this.wonPrelimsCount = wonPrelimsCount;
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getReelLevel() {
        return reelLevel;
    }

    public String getLightLevel() {
        return lightLevel;
    }

    public String getSlipLevel() {
        return slipLevel;
    }

    public String getSingleLevel() {
        return singleLevel;
    }

    public String getTrebleLevel() {
        return trebleLevel;
    }

    public String getHornpipeLevel() {
        return hornpipeLevel;
    }

    public String getTradSetLevel() {
        return tradSetLevel;
    }

    public int getWonPrelimsCount() {
        return wonPrelimsCount;
    }

    public String getUserId() {
        return userId;
    }
}
