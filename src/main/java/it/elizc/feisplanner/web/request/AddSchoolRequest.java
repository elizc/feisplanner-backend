package it.elizc.feisplanner.web.request;

public class AddSchoolRequest {
    private String name;
    private String region;
    private String city;

    public AddSchoolRequest(String name, String region, String city) {
        this.name = name;
        this.region = region;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }
}
