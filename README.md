# How to start
## Initialize DB

```
docker run --name feisplanner_db -d --env POSTGRES_DB=feisplanner --env POSTGRES_USER=feisplanner_user --env POSTGRES_PASSWORD=password -p 5432:5432 postgres:alpine
```

If you already have container for DB

```
docker start feisplanner_db
```